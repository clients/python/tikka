# Tikka

_The rich desktop Python client for Duniter V2 Ğ1 crypto-currency_

**Tikka** is a rich desktop Python client to manage your Ğ1 accounts.

It has a system integrated GUI, is fast and powerful.

It is licenced under the [GPLv3 licence](https://www.gnu.org/licenses/gpl-3.0.en.html) terms.

## Requirements

* [Python](https://www.python.org/) interpreter >= 3.7
* [sqlite3](https://sqlite.org/index.html) database system via [sqlite3](https://docs.python.org/3/library/sqlite3.html) package
* [Qt5](https://www.qt.io/) via [PyQt5](https://www.riverbankcomputing.com/software/pyqt/) package

## Install with pipx

You can use [pipx](https://pypa.github.io/pipx/installation/) to install Tikka.

    pipx install tikka

Then simply run the `tikka` command:

    tikka

Upgrade or uninstall:

    pipx upgrade tikka

    pipx uninstall tikka

## Install/upgrade from PyPI

If you are in a Python virtual environment:

    pip install --upgrade tikka

If you are not:

    # GNU/Linux virtualenv creation
    mkdir tikka
    cd tikka
    python -m venv .venv
    source .venv/bin/activate
    pip install --upgrade tikka

    # get the path of the tikka command
    which tikka

## Run from PyPI installation

    tikka

Or

    python -m tikka

GNU/Linux system requirements:

    sudo apt-get install libsodium23 gcc

## Development 

It is recommended to install and use [pyenv](https://github.com/pyenv/pyenv#installation) to install 
the required Python version.

For Pyinstaller, you need to install Python compiled with the --enable-shared option:

    PYTHON_CONFIGURE_OPTS="--enable-shared" pyenv install 3.7.9

### Setup environment

Tikka internationalization requires gettext to be installed to generate locales binary files.
[Pyinstaller requires some utilities](https://pyinstaller.readthedocs.io/en/stable/requirements.html#requirements).

    sudo apt-get install gettext binutils zip

Install [poetry](https://python-poetry.org/docs/#installation)

At this step, it is highly recommended to setup a virtual environment to isolate Python dependencies of the project from the system and avoid conflicts.
Use [poetry to manage the virtual environment](https://python-poetry.org/docs/managing-environments/).

Install all Python dependencies with `poetry`:

    poetry install --no-root

Copy `bin/pre-commit.sh` for pre-commmit git hooks:

    cp bin/pre-commit.sh .git/hooks/pre-commit

That's it, you should be ready to contribute. Please read `CONTRIBUTING.md`.

### Check code

    make check

### Format code

    make format

### GUI editor

We use QtDesigner to create UI, it will create `.ui` and `.qrc` files that you need to convert to `_rc.py` files with:

    bin/convert_resources.sh

### Internationalization

To update *.po files after code update:

    make i18n_update

To build production *.mo files after *.po have been updated:

    make i18n_build

### Run tests

All tests:

    bin/tests.sh

Only some:

    bin/tests.sh tests/domains/test_signing_key.py

### Run development application

    poetry run python -m tikka

### Release tag

Be sure to test the release with updated dependencies:

    poetry update
    bin/tests.sh

Create a release version commit and tag:

    bin/release.sh VERSION

### Database setup
#### Migrations scripts

Add/Edit scripts in `tikka/assets/migrations` directory:

    # version.name.sql
    00001.init_tables.sql
    00001.init_tables.rollback.sql

#### Migrate commands

    make database
    make database_rollback
    make database_list

### Binary build with pyinstaller

Build standalone executable with [pyinstaller](https://pyinstaller.readthedocs.io/en/stable/index.html):

    make pyinstaller

Zip package will be created in `dist` folder:

    dist/tikka-0.1.0.zip

If Pyinstaller do not find the `libpython` libraries of `pyenv`, try to set the `LD_LIBRARY_PATH` env variable with the `pyenv` lib path:

Example:

    make pyinstaller LD_LIBRARY_PATH=$HOME/.pyenv/versions/3.7.9/lib

### CI/CD

Install `gitlab-runner` to test .gitlab-ci.yml.

To test on a local docker image, specify pull policy:

    gitlab-runner exec docker --docker-pull-policy never tests

### Test on a local gdev node

Run a gdev local node:

    docker run -p 9944:9944 -e DUNITER_VALIDATOR=true duniter/duniter-v2s-gdev-800

Set connexion to url `ws://localhost:9944`.

Use Alice, Bob, Charlie, Dave, Eve and Ferdie accounts derivation with the Dev mnemonic:

    bottom drive obey lake curtain smoke basket hold race lonely fit walk//Alice
