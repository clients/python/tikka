# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os
import shutil
from pathlib import Path

import pytest
from substrateinterface import Keypair, KeypairType

from tikka.adapters.network.rpc.connection import RPCConnection
from tikka.adapters.repository.config import DEFAULT_CONFIG_PATH
from tikka.domains.application import Application
from tikka.domains.entities.account import Account

PYTEST_TMP_FOLDER = Path("/tmp/tikka_test")
# test account address: 5E6taVoL7eW8qpPKjDcf3GjyvFkkMB8EHkkqZksiWeViEkSP
WALLET_MNEMONIC = (
    "album cute glance oppose hub fury strategy health dust rebuild trophy magic"
)
WALLET_MNEMONIC_LANGUAGE_CODE = "en"
WALLET_SEED_HEX = "4c751d1d630ef33e1c3e0e6926a928a5ce229baacedaaccc1c2de15a5443fb1b"
WALLET_PASSWORD = "ABCDEF"

if Path(PYTEST_TMP_FOLDER).exists():
    # remove tmp folder
    shutil.rmtree(PYTEST_TMP_FOLDER)

# create test folder in tmp
try:
    os.mkdir(PYTEST_TMP_FOLDER)
except FileExistsError:
    pass


@pytest.fixture(name="application")
def fixture_application(monkeypatch):
    """
    Return Application instance

    :return:
    """
    # mock network access
    monkeypatch.setattr(RPCConnection, "connect", lambda *args: None)

    application = Application(PYTEST_TMP_FOLDER)
    # clear databases tables
    # check inconsistency between repository and domain list
    repository_list = application.accounts.repository.list()
    domain_list = application.accounts.get_list()
    assert domain_list == repository_list
    for account_ in domain_list:
        application.accounts.repository.delete(account_)
    for category_ in application.categories.list_all():
        application.categories.repository.delete(category_.id)
    for wallet_ in application.wallets.repository.list():
        application.wallets.repository.delete(wallet_.address)
    for node_ in application.nodes.list():
        application.nodes.delete(node_.url)
    for password_ in application.passwords.repository.list():
        application.passwords.repository.delete(password_.root)
    for smith_ in application.smiths.repository.list():
        application.smiths.repository.delete(smith_.identity_index)
    for authority_ in application.authorities.repository.list():
        application.identities.repository.delete(authority_.identity_index)

    # currency parameters
    currency = application.currencies.get_current()
    currency.token_decimals = 2
    currency.token_symbol = "ĞD"
    currency.universal_dividend = 1000
    currency.monetary_mass = 3000
    currency.members_count = 3
    yield application

    application.close()


@pytest.fixture(name="wallet_mnemonic")
def fixture_wallet_mnemonic():
    """
    Return WALLET_MNEMONIC

    :return:
    """
    return WALLET_MNEMONIC


@pytest.fixture(name="wallet_mnemonic_language_code")
def fixture_wallet_mnemonic_language_code():
    """
    Return WALLET_MNEMONIC_LANGUAGE_CODE

    :return:
    """
    return WALLET_MNEMONIC_LANGUAGE_CODE


@pytest.fixture
def wallet_seed_hex():
    """
    Return WALLET_SEED_HEX

    :return:
    """
    return WALLET_SEED_HEX


@pytest.fixture
def wallet_password():
    """
    Return WALLET_PASSWORD

    :return:
    """
    return WALLET_PASSWORD


@pytest.fixture
def account(application: Application, wallet_mnemonic: str):
    """
    Return Account instance from wallet_mnemonic

    :param application: Application instance
    :param wallet_mnemonic: Mnemonic code
    :return:
    """
    keypair = Keypair.create_from_mnemonic(
        wallet_mnemonic, application.currencies.get_current().ss58_format
    )
    return Account(
        address=keypair.ss58_address,
        name="test account",
        balance=1000,
        root=None,
        path=None,
        file_import=False,
        crypto_type=KeypairType.SR25519,
    )


@pytest.fixture
def config_default():
    """
    Return default config dict

    :return:
    """
    with open(DEFAULT_CONFIG_PATH, encoding="utf-8") as file_handler:
        return json.load(file_handler)
