# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from PyQt5.QtCore import QMutex
from pytestqt.qtbot import QtBot
from substrateinterface import Keypair, KeypairType

from tikka.domains.application import Application
from tikka.domains.entities.account import Account
from tikka.slots.pyqt.widgets.account import AccountWidget
from tikka.slots.pyqt.windows.transfer import TransferWindow


def call_count(count):
    """
    Decorator to add the call_count attribute to a function

    :param count: Set call_count value to count
    :return:
    """

    def wrapper(function):
        function.call_count = count
        return function

    return wrapper


def test_account_transfer_to_button(
    qtbot: QtBot,
    application: Application,
    account: Account,
    wallet_mnemonic: str,
    wallet_password: str,
    monkeypatch,
):  # pylint: disable=missing-function-docstring
    application.accounts.add(account)

    # create wallet
    keypair = Keypair.create_from_mnemonic(
        mnemonic=wallet_mnemonic,
        language_code="en",
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    wallet = application.wallets.create(keypair, wallet_password)
    application.wallets.add(wallet)

    widget = AccountWidget(application, account, QMutex())
    qtbot.addWidget(widget)

    assert widget.addressValueLabel.text() == account.address

    # if transfer window is opened, count call, accept window
    @call_count(0)
    def transfer_window_exec_mock(dialog):
        assert isinstance(dialog, TransferWindow)
        transfer_window_exec_mock.call_count = 1
        dialog.accept()

    monkeypatch.setattr(TransferWindow, "exec_", transfer_window_exec_mock)

    widget.transferToButton.click()

    # check if unlock window is called, then the transfer window if the account is unlocked
    assert transfer_window_exec_mock.call_count == 1
