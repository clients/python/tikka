# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import datetime
from unittest.mock import Mock

from pytestqt.qtbot import QtBot
from substrateinterface import Keypair, KeypairType, SubstrateInterface

from tikka.domains.application import Application
from tikka.domains.entities.account import Account
from tikka.domains.entities.identity import IdentityStatus
from tikka.domains.entities.node import Node
from tikka.domains.entities.smith import SmithStatus
from tikka.slots.pyqt.widgets.smith import SmithWidget


def call_count(count):
    """
    Decorator to add the call_count attribute to a function

    :param count: Set call_count value to count
    :return:
    """

    def wrapper(function):
        function.call_count = count
        return function

    return wrapper


def test_smith(
    qtbot: QtBot,
    application: Application,
    account: Account,
    wallet_mnemonic: str,
    wallet_password: str,
    monkeypatch,
):  # pylint: disable=missing-function-docstring

    mnemonic_dev = (
        "bottom drive obey lake curtain smoke basket hold race lonely fit walk"
    )
    derivation_alice = "//Alice"
    derivation_bob = "//Bob"
    derivation_dave = "//Dave"

    identity_index_alice, identity_index_bob, identity_index_dave = (1, 2, 3)

    # create Alice wallet
    keypair_alice = Keypair.create_from_uri(
        suri=mnemonic_dev + derivation_alice,
        language_code="en",
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    wallet_alice = application.wallets.create(keypair_alice, wallet_password)
    application.wallets.add(wallet_alice)

    account_alice = application.accounts.create_new_account(
        name="Alice",
        mnemonic=mnemonic_dev,
        derivation=derivation_alice,
        language_code="en",
        password=wallet_password,
    )

    identity_alice = application.identities.create(
        identity_index_alice,
        int(datetime.datetime.now().timestamp()) + 86400,
        int(datetime.datetime.now().timestamp()) + 86400,
        account_alice.address,
        None,
        IdentityStatus.MEMBER,
    )
    application.identities.add(identity_alice)

    smith_alice = application.smiths.create(
        identity_index=identity_index_alice,
        status=SmithStatus.SMITH,
    )
    application.smiths.add(smith_alice)

    account_bob = application.accounts.create_new_account(
        name="Bob",
        mnemonic=mnemonic_dev,
        derivation=derivation_bob,
        language_code="en",
        password=wallet_password,
    )

    identity_bob = application.identities.create(
        identity_index_bob,
        int(datetime.datetime.now().timestamp()) + 86400,
        int(datetime.datetime.now().timestamp()) + 86400,
        account_bob.address,
        None,
        IdentityStatus.MEMBER,
    )
    application.identities.add(identity_bob)

    smith_bob = application.smiths.create(
        identity_index=identity_index_bob,
        status=SmithStatus.SMITH,
    )
    application.smiths.add(smith_bob)

    # create Dave wallet
    keypair_dave = Keypair.create_from_uri(
        suri="bottom drive obey lake curtain smoke basket hold race lonely fit walk//Dave",
        language_code="en",
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    wallet_dave = application.wallets.create(keypair_dave, wallet_password)
    application.wallets.add(wallet_dave)

    account_dave = application.accounts.create_new_account(
        name="Dave",
        mnemonic=mnemonic_dev,
        derivation=derivation_dave,
        language_code="en",
        password=wallet_password,
    )

    identity_dave = application.identities.create(
        identity_index_dave,
        int(datetime.datetime.now().timestamp()) + 86400,
        int(datetime.datetime.now().timestamp()) + 86400,
        account_dave.address,
        None,
        IdentityStatus.MEMBER,
    )
    application.identities.add(identity_dave)

    ############################
    localhost_node = Node(
        url="ws://localhost:9944",
        peer_id="xxxx",
        software="duniter",
        software_version="v2s",
        session_keys="xxxx",
        block=1234,
    )
    application.nodes.add(localhost_node)
    application.nodes.set_current_url(localhost_node.url)

    # Mock network
    monkeypatch.setattr(application.connections, "is_connected", lambda: True)
    assert application.connections.is_connected()
    application.connections.rpc.client = Mock(SubstrateInterface)
    session_keys = "0xYYYYYYY"

    def rpc_request_side_effect(*args, **kwargs):
        # print("rpc_request")
        # print("args", args)
        # print("kwargs", kwargs)
        if args[0] == "author_rotateKeys":
            return {"result": session_keys}
        return True

    application.connections.rpc.client.rpc_request.side_effect = rpc_request_side_effect

    def query_side_effect(*args, **kwargs):
        # print("query")
        # print("args", args)
        # print("kwargs", kwargs)
        if args[0] == "author_rotateKeys":
            return {"result": session_keys}
        return True

    application.connections.rpc.client.query.side_effect = query_side_effect

    def create_storage_key_side_effect(*args, **kwargs):
        # print("create_storage_key")
        # print("args", args)
        # print("kwargs", kwargs)
        if args[0] == "author_rotateKeys":
            return {"result": session_keys}

    application.connections.rpc.client.create_storage_key.side_effect = (
        create_storage_key_side_effect
    )

    def query_multi_side_effect(*args, **kwargs):
        # print("query_multi")
        # print("args", args)
        # print("kwargs", kwargs)
        if args[0] == "author_rotateKeys":
            return {"result": session_keys}

    application.connections.rpc.client.query_multi.side_effect = query_multi_side_effect

    monkeypatch.setattr(application.smiths.network, "invite", lambda: True)
    monkeypatch.setattr(application.smiths.network, "accept_invitation", lambda: True)
    monkeypatch.setattr(application.smiths.network, "certify", lambda: True)
    monkeypatch.setattr(application.smiths.network, "certify", lambda: True)
    monkeypatch.setattr(
        application.identities, "network_get_identity_indice", lambda: (1, 2, 3)
    )
    monkeypatch.setattr(application.identities, "network_get_identities", lambda: None)

    widget = SmithWidget(application)
    qtbot.addWidget(widget)

    assert widget.accountComboBox.currentData() is None
    assert widget.accountComboBox.model().rowCount() == 4

    # # select Alice account
    # widget.accountComboBox.setCurrentIndex(1)

    # assert widget.accountComboBox.currentData() == account_alice.address
    # assert widget.account is not None
    # assert widget.account.address == account_alice.address
    #
    # assert widget.rotateKeysButton.isEnabled()
    #
    # with qtbot.waitSignal(widget.rotate_keys_timer.timeout, timeout=2000) as _:
    #     # blocker.connect(app.worker.failed)  # Can add other signals to blocker
    #     # Test will block at this point until either the "finished" or the
    #     # "failed" signal is emitted. If 10 seconds passed without a signal,
    #     # TimeoutError will be raised.
    #
    #     # user click on Send button
    #     widget.rotateKeysButton.click()
    #
    # assert widget.sessionKeysTextBrowser.toPlainText() == session_keys

    # assert account_dave.identity_index is not None
    # assert application.identities.is_validated(account_dave.identity_index)
    # assert not application.smiths.exists(account_dave.identity_index)
    #
    # widget.inviteAccountComboBox.setCurrentIndex(1)
    # assert widget.inviteButton.isEnabled()

    # with qtbot.waitSignal(widget.invite_member_timer.timeout, timeout=2000) as _:
    #     # blocker.connect(app.worker.failed)  # Can add other signals to blocker
    #     # Test will block at this point until either the "finished" or the
    #     # "failed" signal is emitted. If 10 seconds passed without a signal,
    #     # TimeoutError will be raised.
    #
    #     # user click on Send button
    #     widget.inviteButton.click()

    # widget.accountComboBox.setCurrentIndex(3)
    # assert widget.account is not None
    # assert widget.account.address == account_dave.address

    # assert application.smiths.get(identity_index_dave).status == ""
    # assert widget.acceptInvitationButton.isEnabled()
