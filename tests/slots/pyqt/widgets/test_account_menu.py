# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from PyQt5.QtCore import QMutex
from PyQt5.QtWidgets import QApplication, QMessageBox
from pytestqt.qtbot import QtBot
from substrateinterface import Keypair, KeypairType

from tikka.domains.application import Application
from tikka.domains.entities.account import Account
from tikka.slots.pyqt.widgets.account_menu import AccountPopupMenu


def test_account_menu_transfer_to(
    qtbot: QtBot,
    application: Application,
    account: Account,
    wallet_mnemonic: str,
    wallet_password: str,
    monkeypatch,
):  # pylint: disable=missing-function-docstring

    # create wallet
    keypair = Keypair.create_from_mnemonic(
        mnemonic=wallet_mnemonic,
        language_code="en",
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    wallet = application.wallets.create(keypair, wallet_password)
    application.wallets.add(wallet)
    application.accounts.unlock(account, wallet_password)

    monkeypatch.setattr(AccountPopupMenu, "transfer_to", lambda *args: None)

    popup_menu = AccountPopupMenu(application, account, QMutex())
    qtbot.addWidget(popup_menu)

    action_change_wallet_password = popup_menu.actions()[0]
    action_change_wallet_password.trigger()


def test_account_menu_transfer_from(
    qtbot: QtBot,
    application: Application,
    account: Account,
    wallet_mnemonic: str,
    wallet_password: str,
    monkeypatch,
):  # pylint: disable=missing-function-docstring

    # create wallet
    keypair = Keypair.create_from_mnemonic(
        mnemonic=wallet_mnemonic,
        language_code="en",
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    wallet = application.wallets.create(keypair, wallet_password)
    application.wallets.add(wallet)

    monkeypatch.setattr(AccountPopupMenu, "transfer_from", lambda *args: None)

    popup_menu = AccountPopupMenu(application, account, QMutex())
    qtbot.addWidget(popup_menu)

    action_change_wallet_password = popup_menu.actions()[1]
    action_change_wallet_password.trigger()


def test_account_menu_copy_to_clipboard(
    qtbot: QtBot, application: Application, account: Account
):  # pylint: disable=missing-function-docstring

    # empty clipboard
    clipboard = QApplication.clipboard()
    clipboard.setText("")
    assert clipboard.text() == ""

    popup_menu = AccountPopupMenu(application, account, QMutex())
    qtbot.addWidget(popup_menu)
    action_copy_to_clipboard = popup_menu.actions()[1]
    action_copy_to_clipboard.trigger()
    assert clipboard.text() == account.address

    # avoid qt slow exit bug https://bugreports.qt.io/browse/QTBUG-32853
    # raise another error: QXcbClipboard: Cannot transfer data, no data available
    clipboard.clear()


def test_account_menu_unlock_account(
    qtbot: QtBot,
    application: Application,
    account: Account,
    wallet_mnemonic: str,
    wallet_password: str,
    monkeypatch,
):  # pylint: disable=missing-function-docstring

    # create wallet
    keypair = Keypair.create_from_mnemonic(
        mnemonic=wallet_mnemonic,
        language_code="en",
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    wallet = application.wallets.create(keypair, wallet_password)
    application.wallets.add(wallet)

    monkeypatch.setattr(AccountPopupMenu, "unlock_account", lambda *args: None)

    popup_menu = AccountPopupMenu(application, account, QMutex())
    qtbot.addWidget(popup_menu)

    action_unlock_account = popup_menu.actions()[2]
    action_unlock_account.trigger()


def test_account_menu_lock_account(
    qtbot: QtBot,
    application: Application,
    account: Account,
    monkeypatch,
    wallet_mnemonic: str,
    wallet_password: str,
):  # pylint: disable=missing-function-docstring

    # create wallet
    keypair = Keypair.create_from_mnemonic(
        mnemonic=wallet_mnemonic,
        language_code="en",
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    wallet = application.wallets.create(keypair, wallet_password)
    application.wallets.add(wallet)
    application.accounts.unlock(account, wallet_password)

    monkeypatch.setattr(AccountPopupMenu, "lock_account", lambda *args: None)
    application.accounts.unlock(account, wallet_password)

    popup_menu = AccountPopupMenu(application, account, QMutex())
    qtbot.addWidget(popup_menu)

    action_save_wallet = popup_menu.actions()[3]
    action_save_wallet.trigger()


def test_account_menu_restore_wallet(
    qtbot: QtBot,
    application: Application,
    account: Account,
    monkeypatch,
):  # pylint: disable=missing-function-docstring

    monkeypatch.setattr(AccountPopupMenu, "wallet_restore", lambda *args: None)
    popup_menu = AccountPopupMenu(application, account, QMutex())
    qtbot.addWidget(popup_menu)

    # mock QMessage confirm dialog
    monkeypatch.setattr(QMessageBox, "question", lambda *args: QMessageBox.Yes)

    action_restore_wallet = popup_menu.actions()[2]
    action_restore_wallet.trigger()


def test_account_menu_forget_wallet(
    qtbot: QtBot,
    application: Application,
    account: Account,
    wallet_mnemonic,
    wallet_password,
    monkeypatch,
):  # pylint: disable=missing-function-docstring

    # create wallet
    keypair = Keypair.create_from_mnemonic(
        mnemonic=wallet_mnemonic,
        language_code="en",
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    wallet = application.wallets.create(keypair, wallet_password)
    application.wallets.add(wallet)

    monkeypatch.setattr(AccountPopupMenu, "wallet_forget", lambda *args: None)
    popup_menu = AccountPopupMenu(application, account, QMutex())
    qtbot.addWidget(popup_menu)

    # mock QMessage confirm dialog
    monkeypatch.setattr(QMessageBox, "question", lambda *args: QMessageBox.Yes)

    action_forget_wallet = popup_menu.actions()[4]
    action_forget_wallet.trigger()


def test_account_menu_wallet_password_forgotten(
    qtbot: QtBot,
    application: Application,
    account: Account,
    wallet_mnemonic: str,
    wallet_password: str,
    monkeypatch,
):  # pylint: disable=missing-function-docstring

    # create wallet
    keypair = Keypair.create_from_mnemonic(
        mnemonic=wallet_mnemonic,
        language_code="en",
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    wallet = application.wallets.create(keypair, wallet_password)
    application.wallets.add(wallet)

    monkeypatch.setattr(
        AccountPopupMenu, "wallet_password_forgotten", lambda *args: None
    )

    popup_menu = AccountPopupMenu(application, account, QMutex())
    qtbot.addWidget(popup_menu)

    wallet_password_forgotten_action = popup_menu.actions()[5]
    wallet_password_forgotten_action.trigger()


def test_account_menu_create_derived_account(
    qtbot: QtBot, application: Application, account: Account, monkeypatch
):  # pylint: disable=missing-function-docstring
    application.accounts.add(account)
    assert len(application.accounts.get_list()) == 1

    monkeypatch.setattr(AccountPopupMenu, "create_derived_account", lambda *args: None)

    popup_menu = AccountPopupMenu(application, account, QMutex())
    qtbot.addWidget(popup_menu)

    action_withdraw_account = popup_menu.actions()[4]
    action_withdraw_account.trigger()


def test_account_menu_rename_account(
    qtbot: QtBot, application: Application, account: Account, monkeypatch
):  # pylint: disable=missing-function-docstring
    application.accounts.add(account)
    assert len(application.accounts.get_list()) == 1

    monkeypatch.setattr(AccountPopupMenu, "rename_account", lambda *args: None)

    popup_menu = AccountPopupMenu(application, account, QMutex())
    qtbot.addWidget(popup_menu)

    action_withdraw_account = popup_menu.actions()[5]
    action_withdraw_account.trigger()


def test_account_menu_withdraw_account(
    qtbot: QtBot, application: Application, account: Account, monkeypatch
):  # pylint: disable=missing-function-docstring
    application.accounts.add(account)
    assert len(application.accounts.get_list()) == 1

    popup_menu = AccountPopupMenu(application, account, QMutex())
    qtbot.addWidget(popup_menu)

    # mock QMessage confirm dialog
    monkeypatch.setattr(QMessageBox, "question", lambda *args: QMessageBox.Yes)

    action_withdraw_account = popup_menu.actions()[6]
    action_withdraw_account.trigger()

    assert len(application.accounts.get_list()) == 0
