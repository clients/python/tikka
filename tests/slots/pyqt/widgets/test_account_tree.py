# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from PyQt5.QtCore import QMutex
from pytestqt.qtbot import QtBot
from substrateinterface import Keypair

from tikka.domains.application import Application
from tikka.domains.entities.account import Account
from tikka.slots.pyqt.widgets.account_tree import AccountTreeWidget


def test_account_tree(
    qtbot: QtBot, application: Application, account: Account
):  # pylint: disable=missing-function-docstring

    # treeview schema :
    #
    # root + category1
    #           + sub_category1
    #           + account
    #               + derived_account
    #      + category2
    #           + sub_category2

    # 2 root categories
    category1 = application.categories.create("Category 1", None)
    category2 = application.categories.create("Category 2", None)
    application.categories.add(category1)
    application.categories.add(category2)

    # 2 sub categories
    sub_category1 = application.categories.create("Sous Catégorie 1", category1.id)
    sub_category2 = application.categories.create("Sous Catégorie 2", category2.id)
    application.categories.add(sub_category1)
    application.categories.add(sub_category2)

    # add account into category1
    account.category_id = category1.id
    application.accounts.add(account)

    # create a derived account from account
    k = Keypair.create_from_uri(
        "album cute glance oppose hub fury strategy health dust rebuild trophy magic//2",
        ss58_format=42,
    )
    derived_account = Account(
        k.ss58_address,
        root="5E6taVoL7eW8qpPKjDcf3GjyvFkkMB8EHkkqZksiWeViEkSP",
        path="//2",
    )
    application.accounts.add(derived_account)

    widget = AccountTreeWidget(application, QMutex())
    qtbot.addWidget(widget)

    assert widget.treeView.model().rowCount(widget.treeView.rootIndex()) == 2
    category1_index = widget.treeView.model().index(0, 0, widget.treeView.rootIndex())
    category2_index = widget.treeView.model().index(1, 0, widget.treeView.rootIndex())
    assert category1_index.internalPointer().element == category1
    assert category2_index.internalPointer().element == category2
    account_index = widget.treeView.model().index(1, 0, category1_index)
    derived_account_index = widget.treeView.model().index(0, 0, account_index)
    sub_category1_index = widget.treeView.model().index(0, 0, category1_index)
    sub_category2_index = widget.treeView.model().index(0, 0, category2_index)
    assert account_index.internalPointer().element == account
    assert derived_account_index.internalPointer().element == derived_account
    assert sub_category1_index.internalPointer().element == sub_category1
    assert sub_category2_index.internalPointer().element == sub_category2


def test_account_tree_expand_state(
    qtbot: QtBot, application: Application, account: Account
):  # pylint: disable=missing-function-docstring

    # treeview schema :
    #
    # root + category1
    #           + account
    #               + derived_account
    #           + sub_category1
    #      + category2
    #           + sub_category2

    # 2 root categories
    category1 = application.categories.create("Category 1", None)
    category2 = application.categories.create("Category 2", None)
    application.categories.add(category1)
    application.categories.add(category2)

    # 2 sub categories
    sub_category1 = application.categories.create("Sous Catégorie 1", category1.id)
    sub_category2 = application.categories.create("Sous Catégorie 2", category2.id)
    application.categories.add(sub_category1)
    application.categories.add(sub_category2)

    # add account into category1
    account.category_id = category1.id
    application.accounts.add(account)

    # create a derived account from account
    k = Keypair.create_from_uri(
        "album cute glance oppose hub fury strategy health dust rebuild trophy magic//2",
        ss58_format=42,
    )
    derived_account = Account(
        k.ss58_address,
        root="5E6taVoL7eW8qpPKjDcf3GjyvFkkMB8EHkkqZksiWeViEkSP",
        path="//2",
    )
    application.accounts.add(derived_account)

    widget = AccountTreeWidget(application, QMutex())
    qtbot.addWidget(widget)

    assert widget.treeView.model().rowCount(widget.treeView.rootIndex()) == 2
    category1_index = widget.treeView.model().index(0, 0, widget.treeView.rootIndex())
    category2_index = widget.treeView.model().index(1, 0, widget.treeView.rootIndex())
    assert category1_index.internalPointer().element == category1
    assert category2_index.internalPointer().element == category2
    account_index = widget.treeView.model().index(1, 0, category1_index)
    derived_account_index = widget.treeView.model().index(0, 0, account_index)
    sub_category1_index = widget.treeView.model().index(0, 0, category1_index)
    sub_category2_index = widget.treeView.model().index(0, 0, category2_index)
    assert account_index.internalPointer().element == account
    assert derived_account_index.internalPointer().element == derived_account
    assert sub_category1_index.internalPointer().element == sub_category1
    assert sub_category2_index.internalPointer().element == sub_category2

    assert widget.treeView.isExpanded(category1_index) is True
    assert widget.treeView.isExpanded(category2_index) is True
    assert category1_index.internalPointer().element.expanded is True
    assert category2_index.internalPointer().element.expanded is True

    # collapse category1
    widget.treeView.collapse(category1_index)
    assert widget.treeView.isExpanded(category1_index) is False
    assert widget.treeView.isExpanded(category2_index) is True
    assert category1_index.internalPointer().element.expanded is False
    assert category2_index.internalPointer().element.expanded is True

    # destroy widget
    del widget

    # new widget
    widget = AccountTreeWidget(application, QMutex())
    qtbot.addWidget(widget)

    assert widget.treeView.model().rowCount(widget.treeView.rootIndex()) == 2
    category1_index = widget.treeView.model().index(0, 0, widget.treeView.rootIndex())
    category2_index = widget.treeView.model().index(1, 0, widget.treeView.rootIndex())
    assert widget.treeView.isExpanded(category1_index) is False
    assert widget.treeView.isExpanded(category2_index) is True
    assert category1_index.internalPointer().element.expanded is False
    assert category2_index.internalPointer().element.expanded is True
