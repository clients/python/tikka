# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from PyQt5.QtWidgets import QApplication, QMessageBox
from pytestqt.qtbot import QtBot

from tikka.domains.application import Application
from tikka.domains.entities.node import Node
from tikka.slots.pyqt.widgets.node_menu import NodePopupMenu


def test_node_menu_copy_peer_id_to_clipboard(
    qtbot: QtBot, application: Application
):  # pylint: disable=missing-function-docstring

    # empty clipboard
    clipboard = QApplication.clipboard()
    clipboard.setText("")
    assert clipboard.text() == ""

    peer_id = "12D3KooWPjTSP5NERhwZnmwUvDtDui5hJxkX6DrV8QnefSaG64Vx"

    node = Node(
        url="ws://localhost:9944",
        peer_id=peer_id,
        block=9999,
        software="Duniter",
        software_version="3.0.0-4cada50-x86_64-linux-gnu",
    )

    popup_menu = NodePopupMenu(application, node)
    qtbot.addWidget(popup_menu)
    action_copy_to_clipboard = popup_menu.actions()[0]
    action_copy_to_clipboard.trigger()
    assert clipboard.text() == peer_id

    # avoid qt slow exit bug https://bugreports.qt.io/browse/QTBUG-32853
    # raise another error: QXcbClipboard: Cannot transfer data, no data available
    clipboard.clear()


def test_node_menu_delete_node(
    qtbot: QtBot, application: Application, monkeypatch
):  # pylint: disable=missing-function-docstring

    peer_id = "12D3KooWPjTSP5NERhwZnmwUvDtDui5hJxkX6DrV8QnefSaG64Vx"

    node = Node(
        url="ws://localhost:9944",
        peer_id=peer_id,
        block=9999,
        software="Duniter",
        software_version="3.0.0-4cada50-x86_64-linux-gnu",
    )
    application.nodes.add(node)
    assert application.nodes.count() == 8

    popup_menu = NodePopupMenu(application, node)
    qtbot.addWidget(popup_menu)

    # mock QMessage confirm dialog
    monkeypatch.setattr(QMessageBox, "question", lambda *args: QMessageBox.Yes)

    action_withdraw_account = popup_menu.actions()[1]
    action_withdraw_account.trigger()

    assert application.nodes.count() == 7
