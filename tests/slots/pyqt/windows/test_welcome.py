# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from pytestqt.qtbot import QtBot

from tikka.domains.application import Application
from tikka.slots.pyqt.windows.welcome import WelcomeWindow


def test_welcome(
    qtbot: QtBot, application: Application
):  # pylint: disable=missing-function-docstring

    window = WelcomeWindow(application)
    qtbot.addWidget(window)
