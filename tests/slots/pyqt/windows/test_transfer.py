# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from unittest.mock import Mock

from PyQt5.QtCore import QEvent, QMutex, Qt
from PyQt5.QtGui import QKeyEvent
from pytestqt.qtbot import QtBot
from substrateinterface import ExtrinsicReceipt

from tikka.domains.application import Application
from tikka.domains.entities.account import Account
from tikka.slots.pyqt.windows.transfer import TransferWindow


def test_transfer_success(
    qtbot: QtBot,
    application: Application,
    wallet_mnemonic: str,
    monkeypatch,
):  # pylint: disable=missing-function-docstring

    # unlock sender account
    application.accounts.create_new_root_account(
        wallet_mnemonic, "en", "test account", "aaaaaa"
    )
    account = application.accounts.get_by_address(
        "5E6taVoL7eW8qpPKjDcf3GjyvFkkMB8EHkkqZksiWeViEkSP"
    )
    assert account is not None

    account.balance = 100

    monkeypatch.setattr(application.accounts.network, "get_balance", lambda *args: 0)

    # press Return event
    event = QKeyEvent(QEvent.KeyPress, Qt.Key_Return, Qt.NoModifier)

    recipient_account = Account(
        "5DJAKEJHAKL2u7rK4RmByQjvc8NQKT7Hu1DJ3YDsSrvjAJ55", balance=0
    )

    window = TransferWindow(application, QMutex(), account)
    qtbot.addWidget(window)

    # Watch for the app.worker.finished signal, then start the worker.
    with qtbot.waitSignal(
        window.fetch_recipient_balance_from_network_async_qworker.finished, timeout=2000
    ) as _:
        # blocker.connect(app.worker.failed)  # Can add other signals to blocker
        # Test will block at this point until either the "finished" or the
        # "failed" signal is emitted. If 10 seconds passed without a signal,
        # TimeoutError will be raised.

        # enter an account address in recipient lineEdit
        window.recipientNameOrAddressLineEdit.setText(recipient_account.address)
        window.recipientNameOrAddressLineEdit.keyPressEvent(event)

    assert window.recipientBalanceValueLabel.text() != ""
    assert window.sendButton.isEnabled() is False
    # selected unit is "units"
    assert window.amountUnitComboBox.currentIndex() == 0
    # set amount value
    window.amountDoubleSpinBox.setValue(2)
    assert window.sendButton.isEnabled() is True
    assert window.transfer_success is None

    # transfer network response mock success
    extrinsic_receipt = Mock(ExtrinsicReceipt)
    extrinsic_receipt.is_success = True
    monkeypatch.setattr(
        application.transfers.network, "send", lambda *args: extrinsic_receipt
    )

    with qtbot.waitSignal(
        window.send_tranfer_to_network_async_qworker.finished, timeout=2000
    ) as _:
        # blocker.connect(app.worker.failed)  # Can add other signals to blocker
        # Test will block at this point until either the "finished" or the
        # "failed" signal is emitted. If 10 seconds passed without a signal,
        # TimeoutError will be raised.

        # user click on Send button
        window.sendButton.click()

    with qtbot.waitSignal(
        window.fetch_sender_balance_from_network_async_qworker.finished, timeout=2000
    ) as _:
        pass

    # transfer success
    assert window.transfer_success is True


def test_transfer_failure(
    qtbot: QtBot,
    application: Application,
    wallet_mnemonic: str,
    monkeypatch,
):  # pylint: disable=missing-function-docstring

    # unlock sender account
    application.accounts.create_new_root_account(
        wallet_mnemonic, "en", "test account", "aaaaaa"
    )
    account = application.accounts.get_by_address(
        "5E6taVoL7eW8qpPKjDcf3GjyvFkkMB8EHkkqZksiWeViEkSP"
    )
    assert account is not None

    account.balance = 100

    monkeypatch.setattr(application.accounts.network, "get_balance", lambda *args: 0)

    # press Return event
    event = QKeyEvent(QEvent.KeyPress, Qt.Key_Return, Qt.NoModifier)

    recipient_account = Account(
        "5DJAKEJHAKL2u7rK4RmByQjvc8NQKT7Hu1DJ3YDsSrvjAJ55", balance=0
    )

    window = TransferWindow(application, QMutex(), account)
    qtbot.addWidget(window)

    # Watch for the app.worker.finished signal, then start the worker.
    with qtbot.waitSignal(
        window.fetch_recipient_balance_from_network_async_qworker.finished, timeout=2000
    ) as _:
        # blocker.connect(app.worker.failed)  # Can add other signals to blocker
        # Test will block at this point until either the "finished" or the
        # "failed" signal is emitted. If 10 seconds passed without a signal,
        # TimeoutError will be raised.

        # enter an account address in recipient lineEdit
        window.recipientNameOrAddressLineEdit.setText(recipient_account.address)
        window.recipientNameOrAddressLineEdit.keyPressEvent(event)

    assert window.recipientBalanceValueLabel.text() != ""
    assert window.sendButton.isEnabled() is False
    # selected unit is "units"
    assert window.amountUnitComboBox.currentIndex() == 0
    # set amount value
    window.amountDoubleSpinBox.setValue(2)
    assert window.sendButton.isEnabled() is True
    assert window.transfer_success is None

    # transfer network response mock failure
    extrinsic_receipt = Mock(ExtrinsicReceipt)
    extrinsic_receipt.is_success = False
    extrinsic_receipt.error_message = {
        "type": "System",
        "name": "BadOrigin",
        "docs": ["error message"],
    }
    monkeypatch.setattr(
        application.transfers.network, "send", lambda *args: extrinsic_receipt
    )

    with qtbot.waitSignal(
        window.send_tranfer_to_network_async_qworker.finished, timeout=2000
    ) as _:
        # blocker.connect(app.worker.failed)  # Can add other signals to blocker
        # Test will block at this point until either the "finished" or the
        # "failed" signal is emitted. If 10 seconds passed without a signal,
        # TimeoutError will be raised.

        # user click on Send button
        window.sendButton.click()

    with qtbot.waitSignal(
        window.fetch_recipient_balance_from_network_async_qworker.finished, timeout=2000
    ) as _:
        pass

    # transfer failed
    assert window.transfer_success is False


def test_transfer_sender_balance_none(
    qtbot: QtBot,
    application: Application,
    wallet_mnemonic: str,
    monkeypatch,
):  # pylint: disable=missing-function-docstring

    # unlock sender account
    application.accounts.create_new_root_account(
        wallet_mnemonic, "en", "test account", "aaaaaa"
    )
    account = application.accounts.get_by_address(
        "5E6taVoL7eW8qpPKjDcf3GjyvFkkMB8EHkkqZksiWeViEkSP"
    )
    assert account is not None

    account.balance = None

    monkeypatch.setattr(application.accounts.network, "get_balance", lambda *args: 0)

    window = TransferWindow(application, QMutex(), account)
    qtbot.addWidget(window)

    assert window.senderBalanceValueLabel.text() == "?"
    assert window.feesButton.isEnabled() is False
    assert window.sendButton.isEnabled() is False
