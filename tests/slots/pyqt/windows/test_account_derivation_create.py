# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from PyQt5.QtWidgets import QLineEdit
from pytestqt.qtbot import QtBot
from substrateinterface import Keypair, KeypairType

from tikka.domains.application import Application
from tikka.domains.entities.account import Account
from tikka.domains.entities.constants import DERIVATION_PATH_MEMBER
from tikka.slots.pyqt.windows.account_derivation_create import (
    AccountDerivationCreateWindow,
)


def test_account_derivation_create(
    qtbot: QtBot,
    application: Application,
    account: Account,
    wallet_mnemonic: str,
    wallet_password: str,
):  # pylint: disable=missing-function-docstring

    # create new root account + password + wallet
    application.accounts.create_new_account(
        mnemonic=wallet_mnemonic,
        language_code="en",
        derivation="",
        name="root account",
        password=wallet_password,
    )

    #########################################################
    # first derivation account should be //0 (member)
    #########################################################
    default_derivation_path = "//0"

    window = AccountDerivationCreateWindow(application, account)
    qtbot.addWidget(window)

    # password display mode on/off
    assert window.mnemonicLineEdit.echoMode() == QLineEdit.Password
    assert window.storedpasswordLineEdit.echoMode() == QLineEdit.Password

    window.showButton.click()

    assert window.mnemonicLineEdit.echoMode() == QLineEdit.Normal
    assert window.storedpasswordLineEdit.echoMode() == QLineEdit.Normal

    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is False

    window.mnemonicLineEdit.setText(wallet_mnemonic)

    with qtbot.waitSignal(window.mnemonic_debounce_timer.timeout, timeout=500) as _:
        pass

    assert (
        window.rootAddressValueLabel.text()
        == "5E6taVoL7eW8qpPKjDcf3GjyvFkkMB8EHkkqZksiWeViEkSP"
    )
    assert window.derivationComboBox.currentData() == default_derivation_path
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is True

    language_code = window.language_code
    derivation_path = window.derivationComboBox.currentData()
    keypair = Keypair.create_from_uri(
        suri=wallet_mnemonic + derivation_path,
        language_code=language_code,
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    assert window.addressValueLabel.text() == keypair.ss58_address
    assert window.storedpasswordLineEdit.text() == wallet_password

    window.buttonBox.button(window.buttonBox.Ok).click()

    derived_account = application.accounts.get_by_address(keypair.ss58_address)
    assert derived_account is not None
    assert derived_account.address == keypair.ss58_address
    assert derived_account.root == account.address
    assert derived_account.path == default_derivation_path

    assert application.wallets.exists(derived_account.address)
    assert application.wallets.is_unlocked(derived_account.address)

    assert application.passwords.exists(derived_account.root)

    #########################################################
    # second derivation account should be //1 (transparent)
    #########################################################
    default_derivation_path = "//1"

    window = AccountDerivationCreateWindow(application, account)
    qtbot.addWidget(window)

    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is False

    window.mnemonicLineEdit.setText(wallet_mnemonic)

    with qtbot.waitSignal(window.mnemonic_debounce_timer.timeout, timeout=500) as _:
        pass

    assert (
        window.rootAddressValueLabel.text()
        == "5E6taVoL7eW8qpPKjDcf3GjyvFkkMB8EHkkqZksiWeViEkSP"
    )
    assert window.derivationComboBox.currentData() == default_derivation_path
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is True

    language_code = window.language_code
    derivation_path = window.derivationComboBox.currentData()
    keypair = Keypair.create_from_uri(
        suri=wallet_mnemonic + derivation_path,
        language_code=language_code,
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    assert window.addressValueLabel.text() == keypair.ss58_address
    assert window.storedpasswordLineEdit.text() == wallet_password

    window.buttonBox.button(window.buttonBox.Ok).click()

    derived_account = application.accounts.get_by_address(keypair.ss58_address)
    assert derived_account is not None
    assert derived_account.address == keypair.ss58_address
    assert derived_account.root == account.address
    assert derived_account.path == default_derivation_path

    assert application.wallets.exists(derived_account.address)
    assert application.wallets.is_unlocked(derived_account.address)

    assert application.passwords.exists(derived_account.root)


def test_account_member_derivation_create(
    qtbot: QtBot,
    application: Application,
    account: Account,
    wallet_mnemonic: str,
    wallet_password: str,
):  # pylint: disable=missing-function-docstring

    # create new root account + password + wallet
    application.accounts.create_new_account(
        mnemonic=wallet_mnemonic,
        language_code="en",
        derivation="",
        name="root account",
        password=wallet_password,
    )

    #########################################################
    # derivation account should be //0 (member)
    #########################################################
    window = AccountDerivationCreateWindow(application, account)
    qtbot.addWidget(window)

    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is False

    window.mnemonicLineEdit.setText(wallet_mnemonic)

    with qtbot.waitSignal(window.mnemonic_debounce_timer.timeout, timeout=500) as _:
        pass

    window.derivationComboBox.setCurrentIndex(0)
    assert (
        window.rootAddressValueLabel.text()
        == "5E6taVoL7eW8qpPKjDcf3GjyvFkkMB8EHkkqZksiWeViEkSP"
    )
    assert window.derivationComboBox.currentData() == DERIVATION_PATH_MEMBER
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is True

    language_code = window.language_code
    derivation_path = window.derivationComboBox.currentData()
    keypair = Keypair.create_from_uri(
        suri=wallet_mnemonic + derivation_path,
        language_code=language_code,
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    assert window.addressValueLabel.text() == keypair.ss58_address
    assert window.storedpasswordLineEdit.text() == wallet_password

    window.buttonBox.button(window.buttonBox.Ok).click()

    derived_account = application.accounts.get_by_address(keypair.ss58_address)
    assert derived_account is not None
    assert derived_account.address == keypair.ss58_address
    assert derived_account.root == account.address
    assert derived_account.path == DERIVATION_PATH_MEMBER

    assert application.wallets.exists(derived_account.address)
    assert application.wallets.is_unlocked(derived_account.address)

    assert application.passwords.exists(derived_account.root)
