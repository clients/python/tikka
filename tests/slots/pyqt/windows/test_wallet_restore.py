# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from PyQt5.QtWidgets import QLineEdit, QMessageBox
from pytestqt.qtbot import QtBot
from substrateinterface import Keypair, KeypairType

from tikka.domains.application import Application
from tikka.domains.entities.account import Account
from tikka.slots.pyqt.windows.wallet_restore import WalletRestoreWindow


def test_wallet_restore_error(
    qtbot: QtBot,
    application: Application,
    wallet_mnemonic: str,
    wallet_password: str,
    account: Account,
):  # pylint: disable=missing-function-docstring

    window = WalletRestoreWindow(application, account)
    qtbot.addWidget(window)

    window.mnemonicLineEdit.setText(wallet_mnemonic[:10])

    with qtbot.waitSignal(window.mnemonic_debounce_timer.timeout, timeout=500) as _:
        pass

    assert window.errorLabel.text() != ""
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is False


def test_wallet_restore(
    qtbot: QtBot,
    application: Application,
    wallet_mnemonic: str,
    account: Account,
):  # pylint: disable=missing-function-docstring

    assert application.wallets.exists(account.address) is False

    window = WalletRestoreWindow(application, account)
    qtbot.addWidget(window)

    # password display mode on/off
    assert window.mnemonicLineEdit.echoMode() == QLineEdit.Password
    assert window.storedpasswordLineEdit.echoMode() == QLineEdit.Password

    window.showButton.click()

    assert window.mnemonicLineEdit.echoMode() == QLineEdit.Normal
    assert window.storedpasswordLineEdit.echoMode() == QLineEdit.Normal

    window.mnemonicLineEdit.setText(wallet_mnemonic)

    with qtbot.waitSignal(window.mnemonic_debounce_timer.timeout, timeout=500) as _:
        pass

    assert window.addressValueLabel.text() == account.address
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is True

    # show password entry field if stored password does not exist
    assert not window.passwordFrame.isHidden()
    assert window.storedPasswordFrame.isHidden()

    password = window.passwordLineEdit.text()

    window.buttonBox.button(window.buttonBox.Ok).click()

    # get keypair from wallet in repository with password
    assert (
        application.wallets.get_keypair_from_wallet(account.address, password)
        is not None
    )
    assert application.wallets.is_unlocked(account.address)


def test_wallet_restore_using_stored_password(
    qtbot: QtBot,
    application: Application,
    wallet_mnemonic: str,
    wallet_password: str,
    account: Account,
):  # pylint: disable=missing-function-docstring

    # create new root account + password + wallet
    application.accounts.create_new_account(
        mnemonic=wallet_mnemonic,
        language_code="en",
        derivation="",
        name="root account",
        password=wallet_password,
    )
    application.wallets.delete(account.address)

    assert application.wallets.exists(account.address) is False

    window = WalletRestoreWindow(application, account)
    qtbot.addWidget(window)

    # password display mode on/off
    assert window.mnemonicLineEdit.echoMode() == QLineEdit.Password
    assert window.storedpasswordLineEdit.echoMode() == QLineEdit.Password

    window.showButton.click()

    assert window.mnemonicLineEdit.echoMode() == QLineEdit.Normal
    assert window.storedpasswordLineEdit.echoMode() == QLineEdit.Normal

    window.mnemonicLineEdit.setText(wallet_mnemonic)

    with qtbot.waitSignal(window.mnemonic_debounce_timer.timeout, timeout=500) as _:
        pass

    assert window.addressValueLabel.text() == account.address
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is True

    # hide password entry field if stored password exists
    assert window.passwordFrame.isHidden()
    assert not window.storedPasswordFrame.isHidden()
    assert window.storedpasswordLineEdit.text() == wallet_password

    window.buttonBox.button(window.buttonBox.Ok).click()

    # get keypair from wallet in repository with password
    assert (
        application.wallets.get_keypair_from_wallet(account.address, wallet_password)
        is not None
    )
    assert application.wallets.is_unlocked(account.address)


def test_wallet_restore_to_reset_password(
    qtbot: QtBot,
    application: Application,
    wallet_mnemonic: str,
    wallet_password: str,
    account: Account,
    monkeypatch,
):  # pylint: disable=missing-function-docstring

    keypair = Keypair.create_from_mnemonic(
        mnemonic=wallet_mnemonic,
        language_code="en",
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    # create wallet in repository with wallet_password
    application.wallets.new(keypair, wallet_password)

    assert application.wallets.exists(account.address) is True

    window = WalletRestoreWindow(application, account, True)
    qtbot.addWidget(window)

    window.mnemonicLineEdit.setText(wallet_mnemonic)

    with qtbot.waitSignal(window.mnemonic_debounce_timer.timeout, timeout=500) as _:
        pass

    assert window.addressValueLabel.text() == account.address
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is True

    # mock QMessage confirm dialog
    monkeypatch.setattr(QMessageBox, "question", lambda *args: QMessageBox.Yes)
    new_password = window.passwordLineEdit.text()

    assert window.storedPasswordFrame.isHidden()
    assert not window.passwordFrame.isHidden()

    window.buttonBox.button(window.buttonBox.Ok).click()

    # get keypair from wallet in repository with new password
    assert (
        application.wallets.get_keypair_from_wallet(account.address, new_password)
        is not None
    )
    assert application.wallets.is_unlocked(account.address)
