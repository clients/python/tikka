# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from pathlib import Path

from duniterpy.key import SigningKey
from pytestqt.qtbot import QtBot
from substrateinterface import KeypairType

from tikka.domains.application import Application
from tikka.domains.entities.wallet import V1FileWallet
from tikka.slots.pyqt.windows.v1_file_import import V1FileImportWindow


def test_v1_file_import(
    qtbot: QtBot,
    application: Application,
    monkeypatch,
):  # pylint: disable=missing-function-docstring

    # pubkey for credentials test/test
    name = secret_id = password_id = "test"
    signing_key = SigningKey.from_credentials(secret_id, password_id)
    address_v1 = "DCovzCEnQm9GUWe6mr8u42JR1JAuoj3HbQUGdCkfTzSr"
    address = "5GAT6CJW8yVKwUuQc7sM5Kk9GZVTpbZYk9PfjNXtvnNgAJZ1"

    v1_file_wallet = V1FileWallet(
        path=Path("dummy path"),
        type="EWIF",
        is_encrypted=True,
        signing_key=signing_key,
    )

    # patch file wallet load
    monkeypatch.setattr(
        application.file_wallets_repository, "load", lambda *args: v1_file_wallet
    )

    window = V1FileImportWindow(application)
    qtbot.addWidget(window)

    window.nameLineEdit.setText(name)
    password = window.passwordLineEdit.text()

    # valid file dummy password
    window.filePasswordLineEdit.setText("xxxx")

    with qtbot.waitSignal(window.debounce_timer.timeout, timeout=500) as _:
        pass

    assert window.v1AddressValueLabel.text() == address_v1
    assert window.addressValueLabel.text() == address
    assert window.errorLabel.text() == ""
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is True

    window.buttonBox.button(window.buttonBox.Ok).click()

    account = application.accounts.get_by_address(address)
    assert account is not None
    assert account.address == address
    assert account.name == name
    assert account.root is None
    assert account.path is None
    assert account.crypto_type == KeypairType.ED25519

    # get keypair from wallet in repository with password
    assert (
        application.wallets.get_keypair_from_wallet(account.address, password)
        is not None
    )
    assert application.wallets.is_unlocked(account.address)
    assert application.passwords.exists(account.address)

    window = V1FileImportWindow(application)
    qtbot.addWidget(window)

    # ERROR ACCOUNT ALREADY EXISTS

    window.nameLineEdit.setText(name)

    # valid file dummy password
    window.filePasswordLineEdit.setText("xxxx")

    with qtbot.waitSignal(window.debounce_timer.timeout, timeout=500) as _:
        pass

    assert window.errorLabel.text() != ""
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is False
