# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from pytestqt.qtbot import QtBot

from tikka.domains.application import Application
from tikka.domains.entities.account import Account
from tikka.slots.pyqt.windows.scan_qr_code_open_cv import ScanQRCodeOpenCVWindow


def test_scan_qrcode(
    qtbot: QtBot, application: Application, account: Account, monkeypatch
):  # pylint: disable=missing-function-docstring

    # CAMERA NOT AVAILABLE
    monkeypatch.setattr(
        ScanQRCodeOpenCVWindow, "get_available_camera_opencv_index", lambda *args: None
    )

    window = ScanQRCodeOpenCVWindow(application)
    qtbot.addWidget(window)

    assert window.errorLabel.text() != ""
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is False
    assert window.addressValueLabel.text() == ""

    # CAMERA AVAILABLE
    monkeypatch.setattr(
        ScanQRCodeOpenCVWindow, "get_available_camera_opencv_index", lambda *args: 0
    )

    #######################################
    # SCAN CANCELED
    def opencv_webcam_qrcode_scanner_mock_cancel():
        """
        Scan has been canceled, return None

        :return:
        """
        return None

    monkeypatch.setattr(
        ScanQRCodeOpenCVWindow,
        "opencv_webcam_qrcode_scanner",
        lambda *args: opencv_webcam_qrcode_scanner_mock_cancel(),
    )
    window = ScanQRCodeOpenCVWindow(application)

    assert window.address is None

    #######################################
    # SCAN SUCCESSFUL

    def opencv_webcam_qrcode_scanner_mock_valid_address():
        """
        Scan return valid address

        :return:
        """
        return "5E6taVoL7eW8qpPKjDcf3GjyvFkkMB8EHkkqZksiWeViEkSP"

    monkeypatch.setattr(
        ScanQRCodeOpenCVWindow,
        "opencv_webcam_qrcode_scanner",
        lambda *args: opencv_webcam_qrcode_scanner_mock_valid_address(),
    )
    window = ScanQRCodeOpenCVWindow(application)

    assert window.address == "5E6taVoL7eW8qpPKjDcf3GjyvFkkMB8EHkkqZksiWeViEkSP"

    qtbot.addWidget(window)

    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is True
    assert window.errorLabel.text() == ""
    assert (
        window.addressValueLabel.text()
        == "5E6taVoL7eW8qpPKjDcf3GjyvFkkMB8EHkkqZksiWeViEkSP"
    )
