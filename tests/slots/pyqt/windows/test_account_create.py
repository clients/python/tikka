# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from pytestqt.qtbot import QtBot
from substrateinterface import Keypair, KeypairType

from tikka.domains.application import Application
from tikka.slots.pyqt.windows.account_create import AccountCreateWindow


def test_account_create(
    qtbot: QtBot, application: Application
):  # pylint: disable=missing-function-docstring

    window = AccountCreateWindow(application)
    qtbot.addWidget(window)

    account_name = "test name"
    window.nameLineEdit.setText(account_name)

    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is False
    assert window.passwordFrame.isHidden()

    window.saveMnemonicCheckBox.setChecked(True)

    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is True
    assert not window.passwordFrame.isHidden()

    mnemonic = window.mnemonicLineEdit.text()
    language_code = window.mnemonicLanguageComboBox.currentData()
    root_keypair = Keypair.create_from_mnemonic(
        mnemonic=mnemonic,
        language_code=language_code,
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )

    window.buttonBox.button(window.buttonBox.Ok).click()

    root_account = application.accounts.get_by_address(root_keypair.ss58_address)
    assert root_account is not None
    assert root_account.name == account_name
    assert root_account.address == root_keypair.ss58_address

    assert application.wallets.exists(root_account.address)
    assert application.wallets.is_unlocked(root_account.address)

    assert application.passwords.exists(root_account.address)
