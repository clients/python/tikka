# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from pytestqt.qtbot import QtBot
from substrateinterface import KeypairType

from tikka.domains.application import Application
from tikka.slots.pyqt.windows.v1_account_import import V1AccountImportWindow


def test_v1_root_account_import(
    qtbot: QtBot, application: Application
):  # pylint: disable=missing-function-docstring

    name = secret_id = password_id = "test"
    address_v1 = "DCovzCEnQm9GUWe6mr8u42JR1JAuoj3HbQUGdCkfTzSr"
    address = "5GAT6CJW8yVKwUuQc7sM5Kk9GZVTpbZYk9PfjNXtvnNgAJZ1"
    window = V1AccountImportWindow(application)
    qtbot.addWidget(window)

    window.secretIDLineEdit.setText(secret_id)
    window.passwordIDLineEdit.setText(password_id)
    window.nameLineEdit.setText(name)

    with qtbot.waitSignal(window.debounce_timer.timeout, timeout=500) as _:
        pass

    assert window.v1AddressValueLabel.text() == address_v1
    assert window.addressValueLabel.text() == address
    assert window.errorLabel.text() == ""
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is True

    password = window.passwordLineEdit.text().strip()

    window.buttonBox.button(window.buttonBox.Ok).click()

    account = application.accounts.get_by_address(address)
    assert account is not None
    assert account.address == address
    assert account.name == name
    assert account.root is None
    assert account.path is None
    assert account.crypto_type == KeypairType.ED25519

    # get keypair from wallet in repository with password
    assert (
        application.wallets.get_keypair_from_wallet(account.address, password)
        is not None
    )
    assert application.wallets.is_unlocked(account.address)
    assert application.passwords.exists(account.address)

    window = V1AccountImportWindow(application)
    qtbot.addWidget(window)

    # ERROR ACCOUNT ALREADY EXISTS
    window.secretIDLineEdit.setText(secret_id)
    window.passwordIDLineEdit.setText(password_id)
    window.nameLineEdit.setText(name)

    with qtbot.waitSignal(window.debounce_timer.timeout, timeout=500) as _:
        pass

    assert window.errorLabel.text() != ""
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is False
