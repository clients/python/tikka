# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from unittest.mock import Mock

from PyQt5.QtCore import QLocale, QMutex
from PyQt5.QtWidgets import QLineEdit
from pytestqt.qtbot import QtBot
from substrateinterface import ExtrinsicReceipt

from tikka.domains.application import Application
from tikka.domains.entities.identity import Identity, IdentityStatus
from tikka.interfaces.adapters.network.accounts import NetworkAccountsInterface
from tikka.interfaces.adapters.network.identities import NetworkIdentitiesInterface
from tikka.interfaces.adapters.network.transfers import NetworkTransfersInterface
from tikka.slots.pyqt.windows.v1_account_import_wizard import (
    V1AccountImportWizardWindow,
)


def test_v1_import_wizard_with_identity(
    qtbot: QtBot,
    application: Application,
    wallet_mnemonic: str,
    wallet_password: str,
    monkeypatch,
):  # pylint: disable=missing-function-docstring

    secret_id = password_id = "test"
    source_account_address_v1 = "DCovzCEnQm9GUWe6mr8u42JR1JAuoj3HbQUGdCkfTzSr"
    source_account_address = "5GAT6CJW8yVKwUuQc7sM5Kk9GZVTpbZYk9PfjNXtvnNgAJZ1"

    source_account = application.accounts.create_new_root_account_v1_from_credentials(
        secret_id, password_id, "test_source", "aaaaaa"
    )
    assert source_account is not None
    assert source_account.balance is None

    # create new V2 root account + password + wallet
    account = application.accounts.create_new_account(
        mnemonic=wallet_mnemonic,
        language_code="en",
        derivation="",
        name="root account",
        password=wallet_password,
    )
    # destination address for //0 derivation
    destination_account_address = "5GTFXi3akAMpdgpPq1VXEY5sMVH5h5nESebQ1TWtqS6tEzYz"
    assert application.accounts.get_by_address(destination_account_address) is None

    assert account is not None
    window = V1AccountImportWizardWindow(application, QMutex())
    window.setLocale(QLocale(application.config.get(application.config.LANGUAGE_KEY)))
    qtbot.addWidget(window)

    # skip PAGE 1 (explanations)
    window.next()

    # PAGE 2 -----------------------------------------------------
    # password display mode on/off
    assert window.sourceSecretIDLineEdit.echoMode() == QLineEdit.Password
    assert window.sourcePasswordIDLineEdit.echoMode() == QLineEdit.Password

    window.sourceShowButton.click()

    assert window.sourceSecretIDLineEdit.echoMode() == QLineEdit.Normal
    assert window.sourcePasswordIDLineEdit.echoMode() == QLineEdit.Normal

    # mock an unknown account with balance None
    application.accounts.network = Mock(NetworkAccountsInterface)
    application.accounts.network.get_balance.side_effect = lambda x: None

    with qtbot.waitSignal(
        window.fetch_source_from_network_async_qworker.finished, timeout=2000
    ) as _:
        # blocker.connect(app.worker.failed)  # Can add other signals to blocker
        # Test will block at this point until either the "finished" or the
        # "failed" signal is emitted. If 10 seconds passed without a signal,
        # TimeoutError will be raised.

        # type in V1 account credentials
        window.sourceSecretIDLineEdit.setText(secret_id)
        window.sourcePasswordIDLineEdit.setText(password_id)

    assert window.sourceV1AddressValueLabel.text() == source_account_address_v1
    assert window.sourceAddressValueLabel.text() == source_account_address
    assert window.wizardPage2.isComplete() is False
    assert window.sourceBalanceValueLabel.text() == ""
    assert window.sourceErrorLabel.text() != ""
    assert window.sourceIdentityValueLabel.text() == ""

    # mock an existing account with not None balance
    assert source_account is not None
    source_account.balance = None
    application.accounts.repository.update(source_account)

    def get_balance(address):
        if address == source_account_address:
            return 1000
        return None

    application.accounts.network = Mock(NetworkAccountsInterface)
    application.accounts.network.get_balance.side_effect = lambda x: get_balance(x)
    application.identities.network = Mock(NetworkIdentitiesInterface)
    application.identities.network.get_identity_index.side_effect = lambda x: 10000
    application.identities.network.get_identity = lambda x: Identity(
        index=x,
        next_creatable_on=1000000,
        removable_on=1000000,
        address=source_account_address,
        old_address=None,
        status=IdentityStatus.MEMBER,
        first_eligible_ud=0,
    )

    with qtbot.waitSignal(
        window.fetch_source_from_network_async_qworker.finished, timeout=10000
    ) as _:
        # blocker.connect(app.worker.failed)  # Can add other signals to blocker
        # Test will block at this point until either the "finished" or the
        # "failed" signal is emitted. If 10 seconds passed without a signal,
        # TimeoutError will be raised.

        # type in V1 account credentials
        window.sourceSecretIDLineEdit.setText("aaaaaaa")
        window.sourcePasswordIDLineEdit.setText("xxxxxxxxx")

    assert "red" in window.sourceErrorLabel.styleSheet()

    with qtbot.waitSignal(
        window.fetch_source_from_network_async_qworker.finished, timeout=10000
    ) as _:
        # blocker.connect(app.worker.failed)  # Can add other signals to blocker
        # Test will block at this point until either the "finished" or the
        # "failed" signal is emitted. If 10 seconds passed without a signal,
        # TimeoutError will be raised.

        # type in V1 account credentials
        window.sourceSecretIDLineEdit.setText(secret_id)
        window.sourcePasswordIDLineEdit.setText(password_id)

    assert window.sourceV1AddressValueLabel.text() == source_account_address_v1
    assert window.sourceAddressValueLabel.text() == source_account_address
    assert "red" not in window.sourceErrorLabel.styleSheet()
    assert window.sourceBalanceValueLabel.text() != ""
    assert window.sourceIdentityValueLabel.text() != ""
    assert window.wizardPage2.isComplete() is True

    assert window.source_account is not None
    assert window.source_account.balance == 1000

    window.next()

    # PAGE 3 -----------------------------------------------------

    assert window.destinationRootNameOrAddressComboBox.currentText() == account.name

    # password display mode on/off
    assert window.destinationMnemonicLineEdit.echoMode() == QLineEdit.Password
    assert window.storedPasswordLineEdit.echoMode() == QLineEdit.Password

    window.destinationShowButton.click()

    assert window.destinationMnemonicLineEdit.echoMode() == QLineEdit.Normal
    assert window.storedPasswordLineEdit.echoMode() == QLineEdit.Normal

    def get_balance_destination(address):
        if address == destination_account_address:
            return 1000
        return None

    # mock an existing account with not None balance
    application.accounts.network = Mock(NetworkAccountsInterface)
    application.accounts.network.get_balance.side_effect = (
        lambda x: get_balance_destination(x)
    )
    application.identities.network = Mock(NetworkIdentitiesInterface)
    application.identities.network.get_identity_index.side_effect = lambda x: None

    with qtbot.waitSignal(
        window.fetch_destination_from_network_async_qworker.finished, timeout=2000
    ) as _:
        # blocker.connect(app.worker.failed)  # Can add other signals to blocker
        # Test will block at this point until either the "finished" or the
        # "failed" signal is emitted. If 10 seconds passed without a signal,
        # TimeoutError will be raised.

        # type in V2 credentials
        window.destinationMnemonicLineEdit.setText(wallet_mnemonic)
        window.destinationMnemonicLanguageComboBox.setCurrentIndex(0)

    assert window.destinationRootAddressValueLabel.text() == account.address
    assert window.destinationDerivationValueLabel.text() == "//0"
    assert window.destinationAddressValueLabel.text() == destination_account_address
    assert window.destinationBalanceValueLabel.text() == "ĞD10.00"
    assert "red" not in window.destinationErrorLabel.styleSheet()
    assert window.wizardPage3.isComplete() is True

    # PAGE 4 ----------------------------------------------------
    assert application.identities.get_by_address(source_account_address) is not None
    assert window.sourceV1AddressValueImportLabel.text() == source_account_address_v1
    assert window.sourceAddressValueImportLabel.text() == source_account_address
    assert (
        window.sourceBalanceValueImportLabel.text()
        == window.sourceBalanceValueLabel.text()
    )
    assert (
        window.sourceIdentityValueImportLabel.text()
        == window.sourceIdentityValueLabel.text()
    )

    assert (
        window.destinationAddressValueImportLabel.text() == destination_account_address
    )
    assert (
        window.destinationBalanceValueImportLabel.text()
        == window.destinationBalanceValueLabel.text()
    )
    assert window.destinationIdentityValueImportLabel.text() == window._("None")

    def get_balance_import(address):
        if address == destination_account_address:
            return 2000
        else:
            return 0

    def get_identity_index(address):
        if address == destination_account_address:
            return 10000
        return None

    application.accounts.network = Mock(NetworkAccountsInterface)
    application.accounts.network.get_balance.side_effect = lambda x: get_balance_import(
        x
    )
    application.identities.network = Mock(NetworkIdentitiesInterface)
    application.identities.network.get_identity_index.side_effect = (
        lambda x: get_identity_index(x)
    )
    application.identities.network.get_identity = lambda x: Identity(
        index=x,
        next_creatable_on=1000000,
        removable_on=1000000,
        address=destination_account_address,
        old_address=None,
        status=IdentityStatus.MEMBER,
        first_eligible_ud=0,
    )

    # transfer network response mock success
    extrinsic_receipt = Mock(ExtrinsicReceipt)
    extrinsic_receipt.is_success = True
    application.transfers.network = Mock(NetworkTransfersInterface)
    application.transfers.network.transfer_all.side_effect = (
        lambda *args: extrinsic_receipt
    )
    application.transfers.network = Mock(NetworkTransfersInterface)
    application.identities.network.change_owner_key.side_effect = lambda *args: True

    with qtbot.waitSignal(
        window.import_source_into_destination_on_network_async_qworker.finished,
        timeout=2000,
    ) as _:
        # blocker.connect(app.worker.failed)  # Can add other signals to blocker
        # Test will block at this point until either the "finished" or the
        # "failed" signal is emitted. If 10 seconds passed without a signal,
        # TimeoutError will be raised.

        # click on import button
        window.importButton.click()

    assert window.sourceV1AddressValueImportLabel.text() == source_account_address_v1
    assert window.sourceAddressValueImportLabel.text() == source_account_address
    assert window.sourceBalanceValueImportLabel.text() == "ĞD0.00"
    assert window.sourceIdentityValueImportLabel.text() != ""

    assert (
        window.destinationAddressValueImportLabel.text() == destination_account_address
    )
    assert window.destinationBalanceValueImportLabel.text() == "ĞD20.00"
    assert window.destinationIdentityValueImportLabel.text() == window._("Member")

    assert window.source_account.balance == 0
    assert window.destination_account.balance == 2000

    assert "green" in window.importErrorLabel.styleSheet()
    assert (
        window.application.accounts.get_by_address(window.destination_account.address)
        is not None
    )
    assert window.wizardPage4.isComplete() is True

    destination_account = application.accounts.get_by_address(
        destination_account_address
    )
    assert destination_account is not None
    assert destination_account.balance == 2000
    assert destination_account.root == account.address
    assert destination_account.path == "//0"


def test_v1_import_wizard_without_identity(
    qtbot: QtBot,
    application: Application,
    wallet_mnemonic: str,
    wallet_password: str,
    monkeypatch,
):  # pylint: disable=missing-function-docstring

    secret_id = password_id = "test"
    source_account_address_v1 = "DCovzCEnQm9GUWe6mr8u42JR1JAuoj3HbQUGdCkfTzSr"
    source_account_address = "5GAT6CJW8yVKwUuQc7sM5Kk9GZVTpbZYk9PfjNXtvnNgAJZ1"

    # create new V2 root account + password + wallet
    account = application.accounts.create_new_account(
        mnemonic=wallet_mnemonic,
        language_code="en",
        derivation="",
        name="root account",
        password=wallet_password,
    )
    # destination address for //1 derivation
    destination_account_address = "5D82RR4Pc9ricQyWzzoYdwMvBsv1DXvkmnNLzkthc2q3hkU5"
    assert application.accounts.get_by_address(destination_account_address) is None

    assert account is not None

    source_account = application.accounts.create_new_root_account_v1_from_credentials(
        secret_id, password_id, "test_source", "aaaaaa"
    )
    assert source_account is not None
    assert source_account.balance is None

    window = V1AccountImportWizardWindow(application, QMutex())
    window.setLocale(QLocale(application.config.get(application.config.LANGUAGE_KEY)))
    qtbot.addWidget(window)

    # skip PAGE 1 (explanations)
    window.next()

    # PAGE 2 -----------------------------------------------------
    # password display mode on/off
    assert window.sourceSecretIDLineEdit.echoMode() == QLineEdit.Password
    assert window.sourcePasswordIDLineEdit.echoMode() == QLineEdit.Password

    window.sourceShowButton.click()

    assert window.sourceSecretIDLineEdit.echoMode() == QLineEdit.Normal
    assert window.sourcePasswordIDLineEdit.echoMode() == QLineEdit.Normal

    # mock an unknown account with balance None
    application.accounts.network = Mock(NetworkAccountsInterface)
    application.accounts.network.get_balance.side_effect = lambda x: None

    with qtbot.waitSignal(
        window.fetch_source_from_network_async_qworker.finished, timeout=2000
    ) as _:
        # blocker.connect(app.worker.failed)  # Can add other signals to blocker
        # Test will block at this point until either the "finished" or the
        # "failed" signal is emitted. If 10 seconds passed without a signal,
        # TimeoutError will be raised.

        # type in V1 account credentials
        window.sourceSecretIDLineEdit.setText(secret_id)
        window.sourcePasswordIDLineEdit.setText(password_id)

    assert window.sourceV1AddressValueLabel.text() == source_account_address_v1
    assert window.sourceAddressValueLabel.text() == source_account_address
    assert window.wizardPage2.isComplete() is False
    assert window.sourceBalanceValueLabel.text() == ""
    assert window.sourceErrorLabel.text() != ""
    assert window.sourceIdentityValueLabel.text() == ""

    # mock an existing account with not None balance
    assert source_account is not None
    source_account.balance = None
    application.accounts.repository.update(source_account)

    def get_balance(address):
        if address == source_account_address:
            return 1000
        return None

    application.accounts.network.get_balance.side_effect = lambda x: get_balance(x)

    with qtbot.waitSignal(
        window.fetch_source_from_network_async_qworker.finished, timeout=2000
    ) as _:
        # blocker.connect(app.worker.failed)  # Can add other signals to blocker
        # Test will block at this point until either the "finished" or the
        # "failed" signal is emitted. If 10 seconds passed without a signal,
        # TimeoutError will be raised.

        # type in V1 account credentials
        window.sourceSecretIDLineEdit.setText("aaaaaaa")
        window.sourcePasswordIDLineEdit.setText("xxxxxx")

    assert "red" in window.sourceErrorLabel.styleSheet()

    with qtbot.waitSignal(
        window.fetch_source_from_network_async_qworker.finished, timeout=2000
    ) as _:
        # blocker.connect(app.worker.failed)  # Can add other signals to blocker
        # Test will block at this point until either the "finished" or the
        # "failed" signal is emitted. If 10 seconds passed without a signal,
        # TimeoutError will be raised.

        # type in V1 account credentials
        window.sourceSecretIDLineEdit.setText(secret_id)
        window.sourcePasswordIDLineEdit.setText(password_id)

    assert window.sourceV1AddressValueLabel.text() == source_account_address_v1
    assert window.sourceAddressValueLabel.text() == source_account_address
    assert window.sourceBalanceValueLabel.text() != ""
    assert "red" not in window.sourceErrorLabel.styleSheet()
    assert window.sourceIdentityValueLabel.text() != ""
    assert window.wizardPage2.isComplete() is True

    assert window.source_account is not None
    assert window.source_account.balance == 1000

    window.next()

    # PAGE 3 -----------------------------------------------------

    assert window.destinationRootNameOrAddressComboBox.currentText() == account.name

    # password display mode on/off
    assert window.destinationMnemonicLineEdit.echoMode() == QLineEdit.Password
    assert window.storedPasswordLineEdit.echoMode() == QLineEdit.Password

    window.destinationShowButton.click()

    assert window.destinationMnemonicLineEdit.echoMode() == QLineEdit.Normal
    assert window.storedPasswordLineEdit.echoMode() == QLineEdit.Normal

    # mock an existing account with not None balance
    def get_balance_destination(address):
        if address == destination_account_address:
            return 1000
        return None

    # mock an existing account with not None balance
    application.accounts.network = Mock(NetworkAccountsInterface)
    application.accounts.network.get_balance.side_effect = (
        lambda x: get_balance_destination(x)
    )
    monkeypatch.setattr(
        application.identities.network, "get_identity_index", lambda *args: None
    )

    with qtbot.waitSignal(
        window.fetch_destination_from_network_async_qworker.finished, timeout=2000
    ) as _:
        # blocker.connect(app.worker.failed)  # Can add other signals to blocker
        # Test will block at this point until either the "finished" or the
        # "failed" signal is emitted. If 10 seconds passed without a signal,
        # TimeoutError will be raised.

        # type in V2 credentials
        window.destinationMnemonicLineEdit.setText(wallet_mnemonic)
        window.destinationMnemonicLanguageComboBox.setCurrentIndex(0)

    assert window.destinationRootAddressValueLabel.text() == account.address
    assert window.destinationDerivationValueLabel.text() == "//1"
    assert window.destinationAddressValueLabel.text() == destination_account_address
    assert window.destinationBalanceValueLabel.text() == "ĞD10.00"
    assert "red" not in window.destinationErrorLabel.styleSheet()
    assert window.wizardPage3.isComplete() is True

    # PAGE 4 ----------------------------------------------------

    assert window.sourceV1AddressValueImportLabel.text() == source_account_address_v1
    assert window.sourceAddressValueImportLabel.text() == source_account_address
    assert (
        window.sourceBalanceValueImportLabel.text()
        == window.sourceBalanceValueLabel.text()
    )
    assert (
        window.sourceIdentityValueImportLabel.text()
        == window.sourceIdentityValueLabel.text()
    )

    assert (
        window.destinationAddressValueImportLabel.text() == destination_account_address
    )
    assert (
        window.destinationBalanceValueImportLabel.text()
        == window.destinationBalanceValueLabel.text()
    )
    assert window.destinationIdentityValueImportLabel.text() == window._("None")

    def get_balance_import(address):
        if address == destination_account_address:
            return 2000
        else:
            return 0

    def get_identity_index(address):
        if address == destination_account_address:
            return 10000
        return None

    application.accounts.network = Mock(NetworkAccountsInterface)
    application.accounts.network.get_balance.side_effect = lambda x: get_balance_import(
        x
    )
    application.identities.network = Mock(NetworkIdentitiesInterface)
    application.identities.network.get_identity_index.side_effect = (
        lambda x: get_identity_index(x)
    )

    # transfer network response mock success
    extrinsic_receipt = Mock(ExtrinsicReceipt)
    extrinsic_receipt.is_success = True
    application.transfers.network = Mock(NetworkTransfersInterface)
    application.transfers.network.transfer_all.side_effect = (
        lambda *args: extrinsic_receipt
    )
    application.transfers.network = Mock(NetworkTransfersInterface)
    application.identities.network.change_owner_key.side_effect = lambda *args: True

    with qtbot.waitSignal(
        window.import_source_into_destination_on_network_async_qworker.finished,
        timeout=2000,
    ) as _:
        # blocker.connect(app.worker.failed)  # Can add other signals to blocker
        # Test will block at this point until either the "finished" or the
        # "failed" signal is emitted. If 10 seconds passed without a signal,
        # TimeoutError will be raised.

        # click on import button
        window.importButton.click()

    assert window.sourceV1AddressValueImportLabel.text() == source_account_address_v1
    assert window.sourceAddressValueImportLabel.text() == source_account_address
    assert window.sourceBalanceValueImportLabel.text() == "ĞD0.00"
    assert window.sourceIdentityValueImportLabel.text() != ""

    assert (
        window.destinationAddressValueImportLabel.text() == destination_account_address
    )
    assert window.destinationBalanceValueImportLabel.text() == "ĞD20.00"
    assert window.destinationIdentityValueImportLabel.text() == window._("None")

    assert window.source_account.balance == 0
    assert window.destination_account.balance == 2000

    assert "green" in window.importErrorLabel.styleSheet()
    assert (
        window.application.accounts.get_by_address(window.destination_account.address)
        is not None
    )
    assert window.wizardPage4.isComplete() is True

    destination_account = application.accounts.get_by_address(
        destination_account_address
    )
    assert destination_account is not None
    assert destination_account.balance == 2000
    assert destination_account.root == account.address
    assert destination_account.path == "//1"
