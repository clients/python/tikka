# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from pytestqt.qtbot import QtBot
from substrateinterface import Keypair, KeypairType

from tikka.domains.application import Application
from tikka.domains.entities.account import Account
from tikka.slots.pyqt.windows.account_unlock import AccountUnlockWindow


def test_account_unlock(
    qtbot: QtBot,
    application: Application,
    account: Account,
    wallet_mnemonic: str,
    wallet_password: str,
):  # pylint: disable=missing-function-docstring

    window = AccountUnlockWindow(application, account)
    qtbot.addWidget(window)

    # create wallet
    keypair = Keypair.create_from_mnemonic(
        mnemonic=wallet_mnemonic,
        language_code="en",
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    wallet = application.wallets.create(keypair, wallet_password)
    application.wallets.add(wallet)

    # display public key
    assert window.addressValueLabel.text() == account.address
    assert window.errorLabel.text() == ""

    # invalid access code
    window.passwordLineEdit.setText("AAAAAA")

    with qtbot.waitSignal(window.debounce_timer.timeout, timeout=500) as _:
        pass

    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is False
    assert window.errorLabel.text() != ""

    # valid access code
    window.passwordLineEdit.setText(wallet_password)

    with qtbot.waitSignal(window.debounce_timer.timeout, timeout=500) as _:
        pass

    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is True
    assert window.errorLabel.text() != ""
