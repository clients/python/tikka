# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from duniterpy.key import SigningKey
from PyQt5.QtWidgets import QLineEdit, QMessageBox
from pytestqt.qtbot import QtBot
from substrateinterface import Keypair, KeypairType

from tikka.domains.application import Application
from tikka.domains.entities.account import Account
from tikka.slots.pyqt.windows.v1_wallet_restore import V1WalletRestoreWindow


def test_v1_wallet_restore_error(
    qtbot: QtBot, application: Application, account: Account
):  # pylint: disable=missing-function-docstring

    account = Account(
        "5GAT6CJW8yVKwUuQc7sM5Kk9GZVTpbZYk9PfjNXtvnNgAJZ1", name="test name"
    )

    window = V1WalletRestoreWindow(application, account)
    qtbot.addWidget(window)

    assert (
        window.addressValueLabel.text()
        == "5GAT6CJW8yVKwUuQc7sM5Kk9GZVTpbZYk9PfjNXtvnNgAJZ1"
    )

    window.secretIDLineEdit.setText("test")
    window.passwordIDLineEdit.setText("bad credential")

    with qtbot.waitSignal(window.debounce_timer.timeout, timeout=500) as _:
        pass

    assert window.errorLabel.text() != ""
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is False


def test_v1_wallet_restore(
    qtbot: QtBot,
    application: Application,
):  # pylint: disable=missing-function-docstring

    secret_id = salt_password = "test"
    v1_address = "DCovzCEnQm9GUWe6mr8u42JR1JAuoj3HbQUGdCkfTzSr"
    address = "5GAT6CJW8yVKwUuQc7sM5Kk9GZVTpbZYk9PfjNXtvnNgAJZ1"

    account = Account(address, name="test name")

    assert application.wallets.exists(account.address) is False

    window = V1WalletRestoreWindow(application, account)
    qtbot.addWidget(window)

    # password display mode on/off
    assert window.secretIDLineEdit.echoMode() == QLineEdit.Password
    assert window.passwordIDLineEdit.echoMode() == QLineEdit.Password
    assert window.storedpasswordLineEdit.echoMode() == QLineEdit.Password

    window.showButton.click()

    assert window.secretIDLineEdit.echoMode() == QLineEdit.Normal
    assert window.passwordIDLineEdit.echoMode() == QLineEdit.Normal
    assert window.storedpasswordLineEdit.echoMode() == QLineEdit.Normal

    window.secretIDLineEdit.setText(secret_id)
    window.passwordIDLineEdit.setText(salt_password)

    with qtbot.waitSignal(window.debounce_timer.timeout, timeout=500) as _:
        pass

    assert window.v1AddressValueLabel.text() == v1_address
    assert window.addressValueLabel.text() == address
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is True

    # show password entry field if stored password does not exist
    assert not window.passwordFrame.isHidden()
    assert window.storedPasswordFrame.isHidden()

    # mock QMessage confirm dialog
    password = window.passwordLineEdit.text()

    window.buttonBox.button(window.buttonBox.Ok).click()

    # get keypair from wallet in repository with password
    assert (
        application.wallets.get_keypair_from_wallet(account.address, password)
        is not None
    )
    assert application.wallets.is_unlocked(account.address)


def test_v1_wallet_restore_using_stored_password(
    qtbot: QtBot, application: Application, wallet_password: str
):  # pylint: disable=missing-function-docstring

    secret_id = salt_password = "test"
    v1_address = "DCovzCEnQm9GUWe6mr8u42JR1JAuoj3HbQUGdCkfTzSr"
    address = "5GAT6CJW8yVKwUuQc7sM5Kk9GZVTpbZYk9PfjNXtvnNgAJZ1"

    account = application.accounts.create_new_root_account_v1_from_credentials(
        secret_id=secret_id,
        password_id=salt_password,
        name="test name",
        password=wallet_password,
    )

    assert account is not None
    application.wallets.delete(account.address)
    assert not application.wallets.exists(account.address)

    window = V1WalletRestoreWindow(application, account)
    qtbot.addWidget(window)

    window.secretIDLineEdit.setText(secret_id)
    window.passwordIDLineEdit.setText(salt_password)

    with qtbot.waitSignal(window.debounce_timer.timeout, timeout=500) as _:
        pass

    assert window.v1AddressValueLabel.text() == v1_address
    assert window.addressValueLabel.text() == address
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is True

    # hide password entry field if stored password exists
    assert window.passwordFrame.isHidden()
    assert not window.storedPasswordFrame.isHidden()
    assert window.storedpasswordLineEdit.text() == wallet_password

    window.buttonBox.button(window.buttonBox.Ok).click()

    # get keypair from wallet in repository with password
    assert (
        application.wallets.get_keypair_from_wallet(account.address, wallet_password)
        is not None
    )
    assert application.wallets.is_unlocked(account.address)


def test_v1_wallet_reset_password(
    qtbot: QtBot,
    application: Application,
    wallet_password: str,
    monkeypatch,
):  # pylint: disable=missing-function-docstring

    secret_id = salt_password = "test"
    v1_address = "DCovzCEnQm9GUWe6mr8u42JR1JAuoj3HbQUGdCkfTzSr"
    address = "5GAT6CJW8yVKwUuQc7sM5Kk9GZVTpbZYk9PfjNXtvnNgAJZ1"
    signing_key = SigningKey.from_credentials(secret_id, salt_password)

    keypair = Keypair.create_from_seed(
        seed_hex=signing_key.seed.hex(),
        ss58_format=application.currencies.get_current().ss58_format,
        crypto_type=KeypairType.ED25519,
    )
    assert keypair.ss58_address == address

    # create wallet in repository with wallet_password
    application.wallets.new(keypair, wallet_password)

    account = Account(address, name="test name")

    assert application.wallets.exists(account.address) is True

    window = V1WalletRestoreWindow(application, account, True)
    qtbot.addWidget(window)

    window.secretIDLineEdit.setText(secret_id)
    window.passwordIDLineEdit.setText(salt_password)

    with qtbot.waitSignal(window.debounce_timer.timeout, timeout=500) as _:
        pass

    assert window.v1AddressValueLabel.text() == v1_address
    assert window.addressValueLabel.text() == address
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is True

    # mock QMessage confirm dialog
    monkeypatch.setattr(QMessageBox, "question", lambda *args: QMessageBox.Yes)
    new_password = window.passwordLineEdit.text()

    assert window.storedPasswordFrame.isHidden()
    assert not window.passwordFrame.isHidden()

    window.buttonBox.button(window.buttonBox.Ok).click()

    # get keypair from wallet in repository with new password
    assert (
        application.wallets.get_keypair_from_wallet(address, new_password) is not None
    )
    assert application.wallets.is_unlocked(account.address)
