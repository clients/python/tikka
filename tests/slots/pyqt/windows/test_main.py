# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from PyQt5.QtCore import QEvent, Qt
from PyQt5.QtGui import QKeyEvent
from pytestqt.qtbot import QtBot

from tikka import __version__
from tikka.domains.application import Application
from tikka.domains.entities.account import Account
from tikka.slots.pyqt.entities.worker import AsyncQWorker
from tikka.slots.pyqt.windows.main import MainWindow


def test_main_window(
    qtbot: QtBot, application: Application, account: Account, monkeypatch
):  # pylint: disable=missing-function-docstring

    monkeypatch.setattr(AsyncQWorker, "start", lambda *args: None)

    window = MainWindow(application)
    qtbot.addWidget(window)

    # check title
    assert __version__ in window.windowTitle()
    assert application.currencies.get_current().name in window.windowTitle()

    # add tab when root account created
    application.accounts.add(account)

    assert window.tabWidget.currentIndex() == 0
    assert window.tabWidget.count() == 1

    # test shortcuts
    # close current tab with Ctrl-w
    event = QKeyEvent(QEvent.KeyPress, Qt.Key_W, Qt.ControlModifier)
    window.keyPressEvent(event)

    assert window.tabWidget.currentIndex() == -1
    assert window.tabWidget.count() == 0
