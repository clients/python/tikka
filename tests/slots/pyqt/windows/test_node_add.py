# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from pytestqt.qtbot import QtBot

from tikka.domains.application import Application
from tikka.domains.entities.node import Node
from tikka.slots.pyqt.windows.node_add import NodeAddWindow


def test_node_add(
    qtbot: QtBot, application: Application, monkeypatch
):  # pylint: disable=missing-function-docstring

    window = NodeAddWindow(application)
    qtbot.addWidget(window)

    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is False
    assert window.errorLabel.text() == ""

    # test network return None (connection error)
    monkeypatch.setattr(
        application.nodes, "network_test_and_get_node", lambda *args: None
    )
    window.testButton.click()
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is False
    assert window.errorLabel.text() != ""

    # test network return Node (connection success)
    node = Node(
        url="url",
        software="software",
        software_version="version",
        peer_id="XXXX",
        block=100,
    )
    monkeypatch.setattr(
        application.nodes, "network_test_and_get_node", lambda *args: node
    )
    window.testButton.click()
    assert window.errorLabel.text() == ""
    assert window.softwareValueLabel.text() == node.software
    assert window.versionValueLabel.text() == node.software_version
    assert window.peerIDValueLabel.text() == node.peer_id
    assert window.blockValueLabel.text() == str(node.block)
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is True
