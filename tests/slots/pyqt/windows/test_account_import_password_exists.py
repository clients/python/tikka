# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from pytestqt.qtbot import QtBot
from substrateinterface import Keypair, KeypairType

from tikka.domains.application import Application
from tikka.domains.entities.account import Account
from tikka.domains.entities.constants import DERIVATION_PATH_MEMBER
from tikka.slots.pyqt.windows.account_import import AccountImportWindow


def test_account_import_root(
    qtbot: QtBot,
    application: Application,
    wallet_mnemonic: str,
    wallet_password: str,
):  # pylint: disable=missing-function-docstring

    # create root account
    application.accounts.create_new_account(
        mnemonic=wallet_mnemonic,
        language_code="en",
        derivation="",
        name="test root",
        password=wallet_password,
    )

    window = AccountImportWindow(application)
    qtbot.addWidget(window)

    window.mnemonicLineEdit.setText(wallet_mnemonic)
    with qtbot.waitSignal(window.mnemonic_debounce_timer.timeout, timeout=500) as _:
        pass

    window.derivationLineEdit.setText("")

    # root account exists already so import is impossible and error message is displayed
    assert window.errorLabel.text() != ""
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is False

    # no stored password
    assert not window.passwordFrame.isHidden()
    assert window.storedPasswordFrame.isHidden()
    assert window.storedpasswordLineEdit.text() == ""


def test_account_import_with_member_derivation(
    qtbot: QtBot,
    application: Application,
    wallet_mnemonic: str,
    wallet_password: str,
    account: Account,
):  # pylint: disable=missing-function-docstring

    # create root account
    application.accounts.create_new_account(
        mnemonic=wallet_mnemonic,
        language_code="en",
        derivation="",
        name="test root",
        password=wallet_password,
    )

    window = AccountImportWindow(application)
    qtbot.addWidget(window)

    suri = wallet_mnemonic + DERIVATION_PATH_MEMBER
    keypair = Keypair.create_from_uri(
        suri=suri,
        ss58_format=application.currencies.get_current().ss58_format,
        crypto_type=KeypairType.SR25519,
        language_code=window.language_code,
    )

    window.mnemonicLineEdit.setText(wallet_mnemonic)
    window.derivationLineEdit.setText(DERIVATION_PATH_MEMBER)

    with qtbot.waitSignal(window.derivation_debounce_timer.timeout, timeout=500) as _:
        pass

    assert window.errorLabel.text() == ""
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is True

    # stored password exists so it is displayed
    assert window.passwordFrame.isHidden()
    assert not window.storedPasswordFrame.isHidden()
    assert window.storedpasswordLineEdit.text() == wallet_password

    window.buttonBox.button(window.buttonBox.Ok).click()

    # get keypair from wallet in repository with password
    assert (
        application.wallets.get_keypair_from_wallet(
            keypair.ss58_address, wallet_password
        )
        is not None
    )
    new_account = application.accounts.get_by_address(keypair.ss58_address)
    assert new_account is not None
    assert new_account.root == account.address
    assert new_account.path == DERIVATION_PATH_MEMBER

    assert application.wallets.exists(new_account.address)
    assert application.wallets.is_unlocked(new_account.address)

    # check read-only root account is created
    root_keypair = Keypair.create_from_mnemonic(
        mnemonic=wallet_mnemonic,
        ss58_format=application.currencies.get_current().ss58_format,
        crypto_type=KeypairType.SR25519,
        language_code=window.language_code,
    )
    root_account = application.accounts.get_by_address(root_keypair.ss58_address)
    assert root_account is not None
    assert root_account.root is None
    assert root_account.path is None

    assert application.wallets.exists(root_account.address)

    assert application.passwords.exists(root_account.address)


def test_account_import_with_transparent_derivation(
    qtbot: QtBot,
    application: Application,
    wallet_mnemonic: str,
    wallet_password: str,
    account: Account,
):  # pylint: disable=missing-function-docstring

    # create root account
    application.accounts.create_new_account(
        mnemonic=wallet_mnemonic,
        language_code="en",
        derivation="",
        name="test root",
        password=wallet_password,
    )

    window = AccountImportWindow(application)
    qtbot.addWidget(window)

    transparent_derivation = "//4"
    suri = wallet_mnemonic + transparent_derivation
    keypair = Keypair.create_from_uri(
        suri=suri,
        ss58_format=application.currencies.get_current().ss58_format,
        crypto_type=KeypairType.SR25519,
        language_code=window.language_code,
    )

    window.mnemonicLineEdit.setText(wallet_mnemonic)
    window.derivationLineEdit.setText(transparent_derivation)

    with qtbot.waitSignal(window.derivation_debounce_timer.timeout, timeout=500) as _:
        pass

    assert window.errorLabel.text() == ""
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is True

    # no stored password
    assert window.passwordFrame.isHidden()
    assert not window.storedPasswordFrame.isHidden()
    assert window.storedpasswordLineEdit.text() == wallet_password

    window.buttonBox.button(window.buttonBox.Ok).click()

    # get keypair from wallet in repository with password
    assert (
        application.wallets.get_keypair_from_wallet(
            keypair.ss58_address, wallet_password
        )
        is not None
    )
    new_account = application.accounts.get_by_address(keypair.ss58_address)
    assert new_account is not None
    assert new_account.root == account.address
    assert new_account.path == transparent_derivation

    assert application.wallets.exists(new_account.address)
    assert application.wallets.is_unlocked(new_account.address)

    # check read-only root account is created
    root_keypair = Keypair.create_from_mnemonic(
        mnemonic=wallet_mnemonic,
        ss58_format=application.currencies.get_current().ss58_format,
        crypto_type=KeypairType.SR25519,
        language_code=window.language_code,
    )
    root_account = application.accounts.get_by_address(root_keypair.ss58_address)
    assert root_account is not None
    assert root_account.root is None
    assert root_account.path is None

    assert application.wallets.exists(root_account.address)
    assert application.passwords.exists(root_account.address)
