# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from pytestqt.qtbot import QtBot

from tikka.domains.application import Application
from tikka.domains.entities.account import Account
from tikka.slots.pyqt.windows.account_rename import AccountRenameWindow


def test_account_rename(
    qtbot: QtBot,
    application: Application,
    account: Account,
):  # pylint: disable=missing-function-docstring

    application.accounts.add(account)
    account_new_name = "test account new name"
    application_account = application.accounts.get_by_address(account.address)
    assert application_account is not None

    window = AccountRenameWindow(application, application_account)
    qtbot.addWidget(window)

    # display public key
    assert window.addressValueLabel.text() == application_account.address
    assert window.nameLineEdit.text().strip() == application_account.name

    # invalid access code
    assert window.buttonBox.button(window.buttonBox.Ok).isEnabled() is True

    # valid access code
    window.nameLineEdit.setText(account_new_name)
    window.buttonBox.button(window.buttonBox.Ok).click()

    assert application_account.name == account_new_name
