# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from pathlib import Path

from duniterpy.key import SigningKey
from PyQt5.QtWidgets import QLineEdit, QMessageBox
from pytestqt.qtbot import QtBot
from substrateinterface import Keypair, KeypairType

from tikka.domains.application import Application
from tikka.domains.entities.account import Account
from tikka.domains.entities.wallet import V1FileWallet
from tikka.slots.pyqt.windows.v1_wallet_restore_from_file import (
    V1WalletRestoreFromFileWindow,
)


def test_v1_wallet_restore_from_file(
    qtbot: QtBot, application: Application, monkeypatch
):  # pylint: disable=missing-function-docstring

    address = "5GAT6CJW8yVKwUuQc7sM5Kk9GZVTpbZYk9PfjNXtvnNgAJZ1"
    secret_id = salt_password = "test"
    signing_key = SigningKey.from_credentials(secret_id, salt_password)

    keypair = Keypair.create_from_seed(
        seed_hex=signing_key.seed.hex(),
        ss58_format=application.currencies.get_current().ss58_format,
        crypto_type=KeypairType.ED25519,
    )
    assert keypair.ss58_address == address

    account = Account(
        address, crypto_type=KeypairType.ED25519, file_import=True, name="test"
    )

    window = V1WalletRestoreFromFileWindow(application, account)
    qtbot.addWidget(window)

    # password display mode on/off
    assert window.filePasswordLineEdit.echoMode() == QLineEdit.Password
    assert window.storedpasswordLineEdit.echoMode() == QLineEdit.Password

    window.showButton.click()

    assert window.filePasswordLineEdit.echoMode() == QLineEdit.Normal
    assert window.storedpasswordLineEdit.echoMode() == QLineEdit.Normal

    assert window.nameValueLabel.text() == account.name
    assert window.addressValueLabel.text() == account.address
    assert window.v1AddressValueLabel.text() == ""

    v1_file_wallet = V1FileWallet(
        path=Path("dummy path"),
        type="EWIF",
        is_encrypted=True,
        signing_key=signing_key,
    )

    # patch file wallet load
    monkeypatch.setattr(
        application.file_wallets_repository, "load", lambda *args: v1_file_wallet
    )

    # mock QMessage confirm dialog
    password = window.passwordLineEdit.text()

    # valid file dummy password
    window.filePasswordLineEdit.setText("xxxx")

    with qtbot.waitSignal(window.debounce_timer.timeout, timeout=500) as _:
        pass

    assert not window.passwordFrame.isHidden()
    assert window.storedPasswordFrame.isHidden()

    window.buttonBox.button(window.buttonBox.Ok).setEnabled(True)
    window.buttonBox.button(window.buttonBox.Ok).click()

    # get keypair from wallet in repository with new password
    assert (
        application.wallets.get_keypair_from_wallet(keypair.ss58_address, password)
        is not None
    )
    assert application.wallets.is_unlocked(account.address)


def test_v1_wallet_restore_from_file_using_stored_password(
    qtbot: QtBot, application: Application, wallet_password: str, monkeypatch
):  # pylint: disable=missing-function-docstring

    address_v1 = "DCovzCEnQm9GUWe6mr8u42JR1JAuoj3HbQUGdCkfTzSr"
    address = "5GAT6CJW8yVKwUuQc7sM5Kk9GZVTpbZYk9PfjNXtvnNgAJZ1"
    secret_id = salt_password = "test"
    signing_key = SigningKey.from_credentials(secret_id, salt_password)

    account = application.accounts.create_new_root_account_v1_from_credentials(
        secret_id=secret_id,
        password_id=salt_password,
        name="test",
        password=wallet_password,
    )

    assert account is not None
    application.wallets.delete(account.address)
    assert not application.wallets.exists(account.address)

    window = V1WalletRestoreFromFileWindow(application, account)
    qtbot.addWidget(window)

    assert window.nameValueLabel.text() == account.name
    assert window.addressValueLabel.text() == account.address
    assert window.v1AddressValueLabel.text() == ""

    v1_file_wallet = V1FileWallet(
        path=Path("dummy path"),
        type="EWIF",
        is_encrypted=True,
        signing_key=signing_key,
    )

    # patch file wallet load
    monkeypatch.setattr(
        application.file_wallets_repository, "load", lambda *args: v1_file_wallet
    )

    # valid file dummy password
    window.filePasswordLineEdit.setText("xxxx")

    with qtbot.waitSignal(window.debounce_timer.timeout, timeout=500) as _:
        pass

    # hide password entry field if stored password exists
    assert window.passwordFrame.isHidden()
    assert not window.storedPasswordFrame.isHidden()
    assert window.storedpasswordLineEdit.text() == wallet_password

    assert window.v1AddressValueLabel.text() == address_v1
    assert window.addressValueLabel.text() == address
    assert window.errorLabel.text() == ""

    window.buttonBox.button(window.buttonBox.Ok).setEnabled(True)
    window.buttonBox.button(window.buttonBox.Ok).click()

    # get keypair from wallet in repository with new password
    assert (
        application.wallets.get_keypair_from_wallet(address, wallet_password)
        is not None
    )
    assert application.wallets.is_unlocked(account.address)


def test_v1_wallet_reset_password_from_file(
    qtbot: QtBot,
    application: Application,
    wallet_password: str,
    monkeypatch,
):  # pylint: disable=missing-function-docstring

    address = "5GAT6CJW8yVKwUuQc7sM5Kk9GZVTpbZYk9PfjNXtvnNgAJZ1"
    secret_id = salt_password = "test"
    signing_key = SigningKey.from_credentials(secret_id, salt_password)

    keypair = Keypair.create_from_seed(
        seed_hex=signing_key.seed.hex(),
        ss58_format=application.currencies.get_current().ss58_format,
        crypto_type=KeypairType.ED25519,
    )
    assert keypair.ss58_address == address

    # create wallet in repository with wallet_password
    application.wallets.new(keypair, wallet_password)

    account = Account(
        address, crypto_type=KeypairType.ED25519, file_import=True, name="test"
    )

    window = V1WalletRestoreFromFileWindow(application, account, True)
    qtbot.addWidget(window)

    assert window.nameValueLabel.text() == account.name
    assert window.addressValueLabel.text() == account.address
    assert window.v1AddressValueLabel.text() == ""

    window.v1_file_wallet = V1FileWallet(
        path=Path("dummy path"),
        type="EWIF",
        is_encrypted=True,
        signing_key=signing_key,
    )

    # mock QMessage confirm dialog
    monkeypatch.setattr(QMessageBox, "question", lambda *args: QMessageBox.Yes)
    new_password = window.passwordLineEdit.text()

    assert window.storedPasswordFrame.isHidden()
    assert not window.passwordFrame.isHidden()

    window.buttonBox.button(window.buttonBox.Ok).setEnabled(True)
    window.buttonBox.button(window.buttonBox.Ok).click()

    # get keypair from wallet in repository with new password
    assert (
        application.wallets.get_keypair_from_wallet(keypair.ss58_address, new_password)
        is not None
    )
    assert application.wallets.is_unlocked(account.address)
