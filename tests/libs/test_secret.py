# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from substrateinterface import Keypair

from tikka.libs.secret import sanitize_mnemonic_string


def test_sanitize_mnemonic(wallet_mnemonic: str, wallet_mnemonic_language_code: str):
    """
    test_sanitize_mnemonic
    :return:
    """
    garbage_mnemonic = wallet_mnemonic.replace(" ", "  \n\n \t\t")
    garbage_mnemonic = f"\t\n   {garbage_mnemonic}  \t  \n  "
    assert not Keypair.validate_mnemonic(
        garbage_mnemonic, wallet_mnemonic_language_code
    )

    sanitized_mnemonic = sanitize_mnemonic_string(garbage_mnemonic)
    assert Keypair.validate_mnemonic(sanitized_mnemonic, wallet_mnemonic_language_code)
