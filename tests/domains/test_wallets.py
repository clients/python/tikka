# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from substrateinterface import Keypair, KeypairType

from tikka.domains.application import Application
from tikka.domains.entities.wallet import Wallet


def test_wallets_create(
    application: Application, wallet_mnemonic: str, wallet_password: str
):
    """
    test_wallets_create
    :return:
    """
    keypair = Keypair.create_from_mnemonic(
        mnemonic=wallet_mnemonic,
        language_code="en",
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    wallet = application.wallets.create(keypair, wallet_password)

    assert isinstance(wallet, Wallet)


def test_wallets_add(
    application: Application, wallet_mnemonic: str, wallet_password: str
):
    """
    test_wallets_add
    :return:
    """
    keypair = Keypair.create_from_mnemonic(
        mnemonic=wallet_mnemonic,
        language_code="en",
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    assert (
        keypair.seed_hex.hex()
        == "4c751d1d630ef33e1c3e0e6926a928a5ce229baacedaaccc1c2de15a5443fb1b"
    )

    wallet = application.wallets.create(keypair, wallet_password)
    application.wallets.add(wallet)
    # get back keypair from encrypted stored seed
    keypair_from_wallet = application.wallets.get_keypair_from_wallet(
        keypair.ss58_address, wallet_password
    )
    assert keypair_from_wallet is not None
    assert keypair.ss58_address == keypair_from_wallet.ss58_address


def test_wallet_get_keypair(
    application: Application, wallet_mnemonic: str, wallet_password: str
):
    """
    test_wallet_get_keypair
    :return:
    """
    keypair = Keypair.create_from_mnemonic(
        mnemonic=wallet_mnemonic,
        language_code="en",
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    assert (
        keypair.seed_hex.hex()
        == "4c751d1d630ef33e1c3e0e6926a928a5ce229baacedaaccc1c2de15a5443fb1b"
    )

    wallet = application.wallets.create(keypair, wallet_password)
    application.wallets.add(wallet)
    # get back keypair from encrypted stored seed
    keypair_from_wallet = application.wallets.get_keypair_from_wallet(
        keypair.ss58_address, wallet_password
    )

    assert keypair_from_wallet is not None
    assert keypair.ss58_address == keypair_from_wallet.ss58_address

    stored_keypair = application.wallets.get_keypair_from_wallet(
        wallet.address, wallet_password
    )
    assert stored_keypair is not None
    assert keypair.ss58_address == stored_keypair.ss58_address
