# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from substrateinterface import Keypair, KeypairType

from tikka.domains.application import Application
from tikka.domains.entities.password import Password


def test_passwords_create(
    application: Application, wallet_mnemonic: str, wallet_password: str
):
    """
    test_wallets_create
    :return:
    """
    keypair = Keypair.create_from_mnemonic(
        mnemonic=wallet_mnemonic,
        language_code="en",
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    password_instance = application.passwords.create(keypair, wallet_password)

    assert isinstance(password_instance, Password)


def test_passwords_get_clear_password(
    application: Application, wallet_mnemonic: str, wallet_password: str
):
    """
    test_wallets_create
    :return:
    """
    keypair = Keypair.create_from_mnemonic(
        mnemonic=wallet_mnemonic,
        language_code="en",
        crypto_type=KeypairType.SR25519,
        ss58_format=application.currencies.get_current().ss58_format,
    )
    password_instance = application.passwords.new(keypair, wallet_password)
    assert isinstance(password_instance, Password)

    clear_password = application.passwords.get_clear_password(keypair)
    assert clear_password is not None
    assert clear_password == wallet_password
