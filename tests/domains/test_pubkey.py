# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from tikka.domains.entities.address import DisplayAddress


def test_display_address():
    """
    test_display_address

    :return:
    """
    address1 = "12BjyvjoAf5qik7R8TKDJAHJugsX23YgJGi2LmBUv2nx"
    shorten1 = "12Bj…v2nx"
    display_address1 = DisplayAddress(address1)
    assert display_address1.shorten == shorten1

    address2 = "J4c8CARmP9vAFNGtHRuzx14zvxojyRWHW2darguVqjtX"
    shorten2 = "J4c8…qjtX"
    display_address2 = DisplayAddress(address2)
    assert display_address2.shorten == shorten2
