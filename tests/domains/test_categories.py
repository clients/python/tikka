# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from tikka.domains.application import Application
from tikka.domains.entities.account import Account


def test_category_delete(application: Application, account: Account):
    """
    test_nodes_add
    :return:
    """
    application.categories.create("Category 1", None)
    # root category
    category1 = application.categories.create("Category 1", None)
    application.categories.add(category1)

    # sub category
    sub_category1 = application.categories.create("Sous Catégorie 1", category1.id)
    application.categories.add(sub_category1)

    # sub sub category
    sub_sub_category1 = application.categories.create(
        "Sous sous Catégorie 1", sub_category1.id
    )
    application.categories.add(sub_sub_category1)

    # add account into category1
    account.category_id = sub_category1.id
    application.accounts.add(account)

    assert account.category_id == sub_category1.id
    assert sub_sub_category1.parent_id == sub_category1.id

    application.categories.delete(sub_category1.id)
    # refresh account properties
    updated_account = application.accounts.get_by_address(account.address)

    assert updated_account is not None
    assert updated_account.category_id == category1.id
    sub_sub_category1 = application.categories.get(sub_sub_category1.id)
    assert sub_sub_category1.parent_id == category1.id
