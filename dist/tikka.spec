# -*- mode: python ; coding: utf-8 -*-


block_cipher = None

LOCALES_SOURCE_PATH = './tikka/locales'
LOCALES_DESTINATION_PATH = 'locales'
IMAGES_SOURCE_PATH = './tikka/slots/gui/images/assets'
IMAGES_DESTINATION_PATH = 'images'
ASSETS_SOURCE_PATH = './tikka/assets'
ASSETS_DESTINATION_PATH = 'assets'

def get_data():
    data = []

    # locales
    for locale in os.listdir(os.path.join(LOCALES_SOURCE_PATH)):
        data.append((
            os.path.join(os.path.abspath(LOCALES_SOURCE_PATH), locale, 'LC_MESSAGES/*.mo'),
            os.path.join(LOCALES_DESTINATION_PATH, locale, 'LC_MESSAGES')
        ))
        data.append((
            os.path.join(os.path.abspath(LOCALES_SOURCE_PATH), locale, 'licence_g1.txt'),
            os.path.join(LOCALES_DESTINATION_PATH, locale)
        ))

    # images
    data.append((os.path.abspath(IMAGES_SOURCE_PATH), IMAGES_DESTINATION_PATH))

    # assets
    data.append((os.path.abspath(ASSETS_SOURCE_PATH), ASSETS_DESTINATION_PATH))

    # dependency module static files
    import mnemonic
    data.append((
        os.path.join(mnemonic.__path__[0], "wordlist", "*.txt"),
        os.path.join("mnemonic","wordlist")
    ))

    return data

a = Analysis(['../tikka/__main__.py'],
             pathex=['dist'],
             binaries=[],
             datas=get_data(),
             hiddenimports=[],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='tikka',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True,
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='tikka')
