#!/usr/bin/env bash

UI_WINDOWS_RESOURCES_PATH=tikka/slots/pyqt/resources/gui/windows
UI_WIDGETS_RESOURCES_PATH=tikka/slots/pyqt/resources/gui/widgets
ICONS_RESOURCES_DIR=tikka/slots/pyqt/resources/icons
ICONS_RESOURCES_PATH=${ICONS_RESOURCES_DIR}/tikka.slots.pyqt.resources.icons.index.qrc

for f in ${UI_WINDOWS_RESOURCES_PATH}/*.ui;
do
  # convert file.ui to file_rc.py
  pyuic5 $f -o ${f%.ui}_rc.py
  # comment obsolete line
  sed -i "s/_translate =/#  _translate =/g" ${f%.ui}_rc.py
  # replace Qt translate by gettext
  sed -i "s/_translate(\".*\", /_(/g" ${f%.ui}_rc.py
done;

for f in ${UI_WIDGETS_RESOURCES_PATH}/*.ui;
do
  pyuic5 $f -o ${f%.ui}_rc.py
  # comment obsolete line
  sed -i "s/_translate =/#  _translate =/g" ${f%.ui}_rc.py
  # replace Qt translate by gettext
  sed -i "s/_translate(\".*\", /_(/g" ${f%.ui}_rc.py
done;

pyrcc5 ${ICONS_RESOURCES_PATH} -o ${ICONS_RESOURCES_DIR}/index_rc.py

