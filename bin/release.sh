#!/usr/bin/env bash

# get current version
TIKKA_VERSION=`poetry version --short`
echo "Current version: ${TIKKA_VERSION}"

if [[ $1 =~ ^[0-9]+.[0-9]+.[0-9]+[0-9a-z]*$ ]]; then
  # update version in package __init__.py
  sed -i "s/__version__ = \".*\"/__version__ = \"$1\"/g" tikka/__init__.py
  sed -i "s/TIKKA_VERSION: \".*\"/TIKKA_VERSION: \"$1\"/g" .gitlab-ci.yml
  # update version in pyproject.toml
  poetry version "$1"
  # commit changes and add version tag
  git commit pyproject.toml tikka/__init__.py .gitlab-ci.yml -m "$1"
  git tag "$1" -a -m "$1"
else
  echo "Wrong version format"
fi
