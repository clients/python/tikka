#!/usr/bin/env bash

# get current version
TIKKA_VERSION=`poetry version --short`

# remove existing folder for current version
if [ -d "./dist/tikka-${TIKKA_VERSION}" ]; then
    rm -r "./dist/tikka-${TIKKA_VERSION}"
fi

# build and zip
poetry run pyinstaller --noconfirm dist/tikka.spec
cd ./dist
mv tikka "tikka-${TIKKA_VERSION}"
zip -r "tikka-${TIKKA_VERSION}.zip" "tikka-${TIKKA_VERSION}"
cd ../.
