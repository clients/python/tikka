#!/usr/bin/env bash

poetry run pytest -q -s -x --disable-warnings $@
