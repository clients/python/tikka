# Copyright 2021 Vincent Texier <vit@free.fr>
#
# This software is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Fonts
ADDRESS_MONOSPACE_FONT_NAME = "Courier"

# icon paths
ICON_ACCOUNT_NO_WALLET = ":/images/locked_no_wallet"
ICON_ACCOUNT_WALLET_LOCKED = ":/images/locked"
ICON_ACCOUNT_WALLET_UNLOCKED = ":/images/unlocked"
ICON_NETWORK_CONNECTED = ":/icons/connected"
ICON_NETWORK_DISCONNECTED = ":/icons/disconnected"
ICON_IDENTITY = ":/icons/member_icon"
ICON_SMITH = ":/icons/smith"
ICON_LOADER = ":/icons/loader"

# no activity delay before executing function when typing in ms
DEBOUNCE_TIME = 300

# preferences keys
SELECTED_TAB_PAGE_PREFERENCES_KEY = "selected_tab_page"
WALLET_LOAD_DEFAULT_DIRECTORY_PREFERENCES_KEY = "wallet_load_default_directory"
WALLET_SAVE_DEFAULT_DIRECTORY_PREFERENCES_KEY = "wallet_save_default_directory"
SELECTED_UNIT_PREFERENCES_KEY = "selected_unit"
ACCOUNTS_TABLE_SORT_COLUMN_PREFERENCES_KEY = "accounts_table_sort_column"
ACCOUNTS_TABLE_SORT_ORDER_PREFERENCES_KEY = "accounts_table_sort_order"
ACCOUNTS_TABLE_CATEGORY_FILTER_PREFERENCES_KEY = "accounts_table_category_filter"
ACCOUNTS_TABLE_WALLET_FILTER_PREFERENCES_KEY = "accounts_table_wallet_filter"
TRANSFER_SENDER_ADDRESS_PREFERENCES_KEY = "transfer_sender_address"
TRANSFER_RECIPIENT_ADDRESS_PREFERENCES_KEY = "transfer_recipient_address"
SMITH_SELECTED_ACCOUNT_ADDRESS = "smith_selected_account_address"
SMITH_INVITE_SELECTED_ACCOUNT_ADDRESS = "smith_invite_selected_account_address"
SMITH_CERTIFY_SELECTED_IDENTITY_INDEX = "smith_certify_selected_identity_index"
TECHNICAL_COMMITTEE_SELECTED_ACCOUNT_ADDRESS = (
    "technical_committee_selected_account_address"
)
