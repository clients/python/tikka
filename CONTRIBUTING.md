# Contribute guide

### Authorization for GitLab CI linter hook
`make gitlab-ci-linter` is failing due to authorization required for CI lint API accesses.
When running this command, just ignore this failed hook.
In case you want to commit a `.gitlab-ci.yml` edition, this hook will prevent the commit creation.
You can [skip the hooks](https://stackoverflow.com/a/7230886) with `git commit -m "msg" --no-verify`.
This is fine for occasional `.gitlab-ci.yml` editions. In case you would like to edit this file more often and have it checked, ask a maintainer to provide you with `GITLAB_PRIVATE_TOKEN` environment variable that can be set into a shell configuration.
With Bash, in `$HOME/.bashrc` add the following:
```bash
export GITLAB_PRIVATE_TOKEN=""
```

With Fish, in `$HOME/.config/fish/config.fish` add the following:
```fish
set -xg GITLAB_PRIVATE_TOKEN ""
```
Check out #169 for more details.

### Black formatting
We are using [Black](https://github.com/psf/black) formatter tool.
Run Black on a Python file to format it:
```bash
poetry run black path/file.py
```
With `autohooks`, Black is called on staged files, so the commit should fail in case black would make changes.
You will have to add Black changes in order to commit your changes.

### Update copyright year
Follow [this documentation](https://github.com/Lucas-C/pre-commit-hooks#removing-old-license-and-replacing-it-with-a-new-one)
Only difference is to update the year in `license_header.txt` rather than `LICENSE.txt`.

## Release workflow
To handle a release, you have to respect this workflow:

* Verify all features and bug fixes are merged in the `main` branch
* Checkout on the `main` branch
* Create a `release_[version]` branch
* Update the `CHANGELOG.md` file and commit
* Create a Merge Request and validate changes on GitLab
* When the Merge Request is approved:
  * Run the `./bin/release.sh` script with the version semantic number as argument:

```bash
./bin/release.sh 0.50.0
```

* A new commit is added with the version number and a tag in git
* Merge the Merge Request release branch to `main` on GitLab
* Release packages from the GitLab pipeline manual job of the `main` branch
