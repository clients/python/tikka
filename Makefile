.PHONY: format format_check check mypy ruff isort insert-license gitlab-ci-linter i18n_update i18n_build database database_rollback database_list build pyinstaller

SHELL := /bin/bash

format:
	poetry run black tikka
	poetry run black tests

format_check:
	poetry run black --check tikka
	poetry run black --check tests

# check static typing
mypy:
	poetry run mypy --ignore-missing-imports tikka
	poetry run mypy --ignore-missing-imports tests

# check code errors
ruff:
	poetry run ruff tikka
	poetry run ruff tests

# check import sorting
isort:
	poetry run isort -sg *_rc.py -rc tikka tests

# check licence headers
insert-license:
	poetry run insert_license --license-filepath=license_header.txt $$(find $$(pwd -P)/tikka -type f | grep +*.py$$ | grep -v +*_rc.py$ | sed -z "s/\n/ /g")
	poetry run insert_license --license-filepath=license_header.txt $$(find $$(pwd -P)/tests -type f | grep +*.py$$ | sed -z "s/\n/ /g")

# check gitlab-ci
gitlab-ci-linter:
	poetry run gitlab-ci-linter --server https://git.duniter.org --project clients/python/tikka --filename .gitlab-ci.yml

# full check
check: insert-license isort format mypy ruff gitlab-ci-linter

i18n_update:
	find ./tikka -type f | grep .py$$ | grep -v __init__.py$$ | xargs xgettext --debug -ktr -L Python --from-code utf-8 --output-dir=./tikka/locales -d application
	msgmerge ./tikka/locales/fr_FR/LC_MESSAGES/application.po tikka/locales/application.po --update
	msgmerge ./tikka/locales/en_US/LC_MESSAGES/application.po tikka/locales/application.po --update

i18n_build:
	msgfmt tikka/locales/fr_FR/LC_MESSAGES/application.po  --output-file=tikka/locales/fr_FR/LC_MESSAGES/application.mo
	msgfmt tikka/locales/en_US/LC_MESSAGES/application.po  --output-file=tikka/locales/en_US/LC_MESSAGES/application.mo

database:
	for file in ${HOME}/.config/tikka/*.sqlite3; do poetry run yoyo apply --batch --no-config-file --database sqlite:///$${file} ./tikka/adapters/repository/assets/migrations; done

database_rollback:
	for file in ${HOME}/.config/tikka/*.sqlite3; do poetry run yoyo rollback --batch --no-config-file --database sqlite:///$${file} ./tikka/adapters/repository/assets/migrations; done

database_list:
	for file in ${HOME}/.config/tikka/*.sqlite3; do poetry run yoyo list --batch --no-config-file --database sqlite:///$${file} ./tikka/adapters/repository/assets/migrations; done

build:
	./bin/convert_resources.sh
	msgfmt tikka/locales/fr_FR/LC_MESSAGES/application.po  --output-file=tikka/locales/fr_FR/LC_MESSAGES/application.mo
	msgfmt tikka/locales/en_US/LC_MESSAGES/application.po  --output-file=tikka/locales/en_US/LC_MESSAGES/application.mo
	poetry build

pyinstaller:
	./bin/pyinstaller.sh
